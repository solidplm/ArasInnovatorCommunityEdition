@echo off
call :StartTimer

rem Get start time:
for /F "tokens=1-4 delims=:.," %%a in ("%time%") do (
   set /A "start=(((%%a*60)+1%%b %% 100)*60+1%%c %% 100)*100+1%%d %% 100"
)

del /s /q LocalExportPackages

if exist .\LocalExportPackages\LocalExport.log del .\LocalExportPackages\LocalExport.log
.\AutomatedProcedures\tools\PackageImportExportUtilities\consoleUpgrade\consoleUpgrade.exe server=http://localhost/ArasInnovatorCommunityEdition database=ArasInnovatorCommunityEdition login=root password=innovator release="2023" dir=".\LocalExportPackages" log=.\LocalExportPackages\LocalExport.log

xcopy ".\LocalExportPackages\*.*" ".\AML-packages" /i /c /k /e /r /y /exclude:_exclude_files.txt

call :StopTimer
rem Get end time:
for /F "tokens=1-4 delims=:.," %%a in ("%time%") do (
   set /A "end=(((%%a*60)+1%%b %% 100)*60+1%%c %% 100)*100+1%%d %% 100"
)

rem Get elapsed time:
set /A elapsed=end-start

rem Show elapsed time:
set /A hh=elapsed/(60*60*100), rest=elapsed%%(60*60*100), mm=rest/(60*100), rest%%=60*100, ss=rest/100, cc=rest%%100
if %mm% lss 10 set mm=0%mm%
if %ss% lss 10 set ss=0%ss%
rem if %cc% lss 10 set cc=0%cc%
rem echo %hh%:%mm%:%ss%,%cc%
echo Started: %StartTime%
echo Stopped: %StopTime%
echo Elapsed: %hh%:%mm%:%ss%

:StartTimer
:: Store start time
set StartTIME=%TIME%
goto :EOF

:StopTimer
:: Get the end time
set StopTIME=%TIME%
goto :EOF