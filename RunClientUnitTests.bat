@echo off

echo RunClientUnitTests.bat:
echo Target audience: Developers
echo Purpose: Run JavaScript unit tests

SET PathToThisBatFileFolder=%~dp0

SET WorkingDirectory=%CD%
CD /D "%PathToThisBatFileFolder%"

"%PathToThisBatFileFolder%AutomatedProcedures\tools\nant-0.92\bin\nant.exe" ^
	"/f:%PathToThisBatFileFolder%AutomatedProcedures\NantScript.xml" ^
	Parse.AML.Methods.JS

CD Tests\ClientTests\
CALL ..\..\AutomatedProcedures\tools\NodeJS\node_modules\.bin\karma.cmd start

CD /D "%WorkingDirectory%"
pause