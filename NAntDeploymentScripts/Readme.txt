This folder contains NAnt scripts which will be perform during
deploy procedure in the next places:
	1. Before Upgrade CodeTree
	2. After Upgrade CodeTree
	3. Before Upgrade DB
	4. After Upgrade DB

Therefore the scripts for each of this case are located in 
the following folders:
	1. 1-PreUpgradeCodeTree
	2. 2-PostUpgradeCodeTree
	3. 3-PreUpgradeDB
	4. 4-PostUpgradeDB

! If there are not a necessary folder you can create it 
with the exact name as in the above list.

The template of such Script are located near this document.