@echo off

echo ContinuousIntegration.bat:
echo Target audience: Build engeneer
echo Purpose: Run analog of ContinuousIntegration build locally

SET PathToThisBatFileFolder=%~dp0
CALL "%PathToThisBatFileFolder%AutomatedProcedures\BatchUtilityScripts\CheckAdminPrivileges.bat 
IF errorlevel 1 GOTO END
CALL "%PathToThisBatFileFolder%AutomatedProcedures\BatchUtilityScripts\GetMachineSpecificIncludes.bat
IF errorlevel 1 GOTO END

SET WorkingDirectory=%CD%
CD /D "%PathToThisBatFileFolder%"

FOR /f "delims=" %%i in ('git rev-parse --abbrev-ref HEAD') DO SET BranchName=%%i

"%PathToThisBatFileFolder%AutomatedProcedures\tools\nant-0.92\bin\nant.exe" ^
	"/f:%PathToThisBatFileFolder%AutomatedProcedures\NantScript.xml" ^
	ContinuousIntegration ^
	-D:Branch.Name="%BranchName%" ^
	-D:MachineSpecific.Includes.Folder.Path=%MachineSpecificIncludesPath% ^
	-D:Is.MachineSpecific.Includes.Mandatory=true

:END
if not errorlevel 1 (
	powershell write-host -foregroundcolor green "SUCCESS!!!"
) else (
	powershell write-host -foregroundcolor red "FAILURE!!!"
)

CD /D "%WorkingDirectory%"
pause