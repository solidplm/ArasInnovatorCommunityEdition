@echo off

echo *********************
echo Restarting IIS Server
echo *********************
iisreset

echo *********************************************************************************************************************************************************************
echo You are going to setup new Innovator instance in local IIS using this repository as a code tree. Adjust configuration files to work with this instance.
echo *********************************************************************************************************************************************************************

SET PathToThisBatFileFolder=%~dp0
CALL "%PathToThisBatFileFolder%AutomatedProcedures\BatchUtilityScripts\CheckAdminPrivileges.bat 
IF errorlevel 1 GOTO END
CALL "%PathToThisBatFileFolder%AutomatedProcedures\BatchUtilityScripts\GetMachineSpecificIncludes.bat 
IF errorlevel 1 GOTO END

SET WorkingDirectory=%CD%
CD /D "%PathToThisBatFileFolder%"

@REM Remove trailing backslash as it is interpreted as invalid character by NAnt
SET PathToThisBatFileFolder=%PathToThisBatFileFolder:~0,-1%

FOR /f "delims= " %%i in ('wmic computersystem get name') DO FOR /f "delims=" %%t in ("%%i") DO SET LocalMachineName=%%t
FOR /f "delims=" %%i in ('git rev-parse --abbrev-ref HEAD') DO SET BranchName=%%i

AutomatedProcedures\tools\nant-0.92\bin\nant.exe ^
	"/f:AutomatedProcedures\NantScript.xml" ^
	SetupParameters.For.Developer.Environment ^
	Restore.Innovator.DB ^
	GetGitDiffBetweenCurrentHeadStateAndLastRelease ^
	Pre.Upgrade.CodeTree ^
	Post.Upgrade.CodeTree ^
	Upgrade.DB ^
	Print.Url.Of.Installed.Innovator ^
	-D:Branch.Name="%BranchName%" ^
	-D:Local.Machine.Name=%LocalMachineName% ^
	-D:Path.To.Installed.Innovator="%PathToThisBatFileFolder%" ^
	-D:MachineSpecific.Includes.Folder.Path=%MachineSpecificIncludesPath% ^
	-D:Is.MachineSpecific.Includes.Mandatory=true

:END
if not errorlevel 1 (
	powershell write-host -foregroundcolor green "SUCCESS!!!"
) else (
	powershell write-host -foregroundcolor red "FAILURE!!!"
)

CD /D "%WorkingDirectory%"
pause