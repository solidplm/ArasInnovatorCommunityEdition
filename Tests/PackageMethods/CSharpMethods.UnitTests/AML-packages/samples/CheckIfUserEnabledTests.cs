﻿using NUnit.Framework;
using CSharpMethods.Methods.CheckIfUserEnabled;
using Aras.IOM;
using Aras.Server.Core;
using System.Xml;
using NSubstitute;
using System.Globalization;

namespace CSharpMethods.UnitTests.AMLPackages.samples
{
    [TestFixture]
    public class CheckIfUserEnabledTests
    {

        const string positiveResponseWithLogonEnabled =
                @"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"">
                    <SOAP-ENV:Body>
                        <Result>
                            <Item type='User'>
                                <logon_enabled>1</logon_enabled>
                            </Item>
                        </Result>
                    </SOAP-ENV:Body>
                </SOAP-ENV:Envelope>";

        const string positiveResponseWithLogonDisabled =
                @"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"">
                    <SOAP-ENV:Body>
                        <Result>
                            <Item type='User'>
                                <logon_enabled>0</logon_enabled>
                            </Item>
                        </Result>
                    </SOAP-ENV:Body>
                </SOAP-ENV:Envelope>";

        const string errorMessage = "No items of type User found.";
        string faultString = string.Format(CultureInfo.InvariantCulture,
            @"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"">
                <SOAP-ENV:Body>
                    <SOAP-ENV:Fault xmlns:af=""http://www.aras.com/InnovatorFault"">
                        <faultcode>SOAP-ENV:Server</faultcode>
                        <faultstring><![CDATA[{0}]]></faultstring>
                        <detail></detail>
                    </SOAP-ENV:Fault>
                </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>", errorMessage);

        [Test]
        public static void CheckThatInnovatorServerExceptionThrownWhenContextItemIsEmpty()
        {
            const string expectedExceptionMessage = "login_name is not defined";

            IServerConnection serverConnection = NSubstitute.Substitute.For<IServerConnection>();
            ItemMethod itemMethod = new ItemMethod(serverConnection);
            InnovatorServerException exception = Assert.Throws<InnovatorServerException>(() => itemMethod.methodCode());
            Assert.AreEqual(expectedExceptionMessage, exception.Message);

            string amlOfContextItem =
                @"<AML>
                    <Item type='User'>
                        <login_name></login_name>
                    </Item>
                </AML>";
            itemMethod.loadAML(amlOfContextItem);
            exception = Assert.Throws<InnovatorServerException>(() => itemMethod.methodCode());
            Assert.AreEqual(expectedExceptionMessage, exception.Message);
        }

        [Test]
        public void CheckThatMethodReturnsOriginalErrorFromServer()
        {
            IServerConnection serverConnection = Substitute.For<IServerConnection>();
            serverConnection
                .When(x => x.CallAction(Arg.Any<string>(), Arg.Any<XmlDocument>(), Arg.Any<XmlDocument>()))
                .Do(x => (x[2] as XmlDocument).LoadXml(faultString));

            ItemMethod itemMethod = new ItemMethod(serverConnection);
            string amlOfContextItem =
                @"<AML>
                    <Item type='User'>
                        <login_name>sample_user</login_name>
                    </Item>
                </AML>";
            itemMethod.loadAML(amlOfContextItem);

            Item result = itemMethod.methodCode();
            Assert.IsTrue(result.isError());
            Assert.AreEqual(errorMessage, result.getErrorString());
        }
    }
}
