﻿namespace IntegrationTests
{
	public class CustomIntegrationTestsBaseClass : Aras.Tests.Integration.InnovatorServerTests
	{
		protected override string RelativePathToIntegrationTestsDirectory => "..\\..\\IntegrationTests\\";
		protected override string CustomRelativePathToIntegrationTestsTestCasesDirectory => "IntegrationTests";
		protected override string DefaultRelativePathToIntegrationTestsTestCasesDirectory => "TestCases";
	}
}
