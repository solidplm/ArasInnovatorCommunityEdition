﻿using Aras.Tests.Integration;
using NUnit.Framework;

namespace IntegrationTests
{
	/// <summary>
	/// 
	/// </summary>
	[SetUpFixture]
	public class MyIntegrationTestsSetup : IntegrationTestsSetup
	{
		/// <summary>
		/// 
		/// </summary>
		[SetUp]
		public override void RunBeforeAnyTests()
		{
			base.RunBeforeAnyTests();
		}

		/// <summary>
		/// 
		/// </summary>
		[TearDown]
		public override void RunAfterAnyTests()
		{
			base.RunAfterAnyTests();
		}
	}
}