Assuming that you just clonned this repository before you can start to work you need local instance of Innovator.
To setup local instance of Innovator you need to complete the following steps:

Prerequisites to setup innovator locally:
	a) IIS
	b) MSSQL Server 2008 R2 or higher. Can be either local instance or remote.
	c) Baseline database backup - "ftp://ftp.aras.com/InnovatorBuilds/Released/11.0 SP9/11.0 SP9 CD Image/CoreAndSolutions110.bak".
	
Prerequisites to run tests and ContinuousIntegration locally:
	d) Microsoft Visual Studio 2015 (Professional or Enterprise edition) or Microsoft Build Tools 2015 (Free, can be downloaded from Microsoft web site).
	e) Baseline code tree backup - "ftp://ftp.aras.com/InnovatorBuilds/Released/11.0 SP9/Internal/CodeTree.zip".
	f) NodeJs (minimum version is 3.x due to Windows limitation of file name length). Latest version can be downloaded from https://nodejs.org

1) Checkout the latest development branch. For example:
	git checkout dev-sprint-1

2) Run SetupInnovatorHere.bat as Administartor - This will perform the following steps:
	- Use your machine and branch specific settings file (will create it if it doesn't exist and will ask you to modify its values)
	- Verify your configuration (This step can lead to failures if configuration has errors. In case of errors fix them and re-run the SetupInnovatorHere.bat as Administartor)
	- Create Web Application in local IIS with which you can work
	- Restore Innovator database from backup which specified in the 'Path.To.DB.Bak' property
	- Upgrade restored DB with all the necessary AMLs since the starting tag
	The name of the instance is a http://localhost/[branch_name]. For example http://localhost/dev-sprint-1
3) Navigate to just created instance using browser (the link will be provided in SetupInnovatorHere.bat output). You can also navigate to the 'IIS Manager' and locate new instance in expanded 'Default Web Site'