@echo off

echo RunTests.bat
echo Target audience: QA team, Development team
echo Purpose: Run Unit tests.

SET PathToThisBatFileFolder=%~dp0

"%PathToThisBatFileFolder%AutomatedProcedures\tools\nant-0.92\bin\nant.exe" ^
	"/f:%PathToThisBatFileFolder%AutomatedProcedures\NantScript.xml" ^
	RunUnitTests

if not errorlevel 1 (
	powershell write-host -foregroundcolor green "SUCCESS!!!"
) else (
	powershell write-host -foregroundcolor red "FAILURE!!!"
)

pause