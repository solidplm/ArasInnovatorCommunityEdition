﻿<AML>
 <Item type="Method" id="9A9C5399DC964A8E8AEACB157DE08F2D" action="add">
  <execution_allowed_to keyed_name="World" type="Identity">A73B655731924CD0B027E4F4D5FCC0A9</execution_allowed_to>
  <method_code><![CDATA[            /* *****************************************************************************
                        Method Name:			add_associated_items
                        Created By/Company:		ChitHsu 
                        Creation Date:			2018-10-31
                        Description:
                            Add Associated_Item of Affected_Item.

                        Hooks:
                            Type:		ECR 
                            Event:		OnAfterAdd
                            Called by:	<Parent functions>

                        Inputs:
                            <describe the inputs expected by the server method>

                        Revisions:
                            Rev Date		Modified By			Description
                            2018-10-30		Sandar				Using Get_Associated_Item Method
                            2019-11-22		Sandar				Delete Associated Item if there is no Affected Item related with it.
                            2019-12-30      Sandar              Code Refactoring
                        ***************************************************************************** */

            //WARNING: DO NOT FORGET TO DISABLE THIS IN A PRODUCTION ENVIRONMENT!!!
            // if (System.Diagnostics.Debugger.Launch()) System.Diagnostics.Debugger.Break();

            string MethodName = "add_associated_items";

            //***** GRANT IDENTITY PRIVILEGES
            // grantIdentityPrivileges("Super User"); // If this is enabled always revoke the privileges in the Finally below.
            //*******************************
            Innovator myInnovator = this.getInnovator();
            Item result = this; // default return value.

            try
            {
                string ecr_id = this.getProperty("id", "");
                string Affected_id = "";
                string rev = "";

                this.fetchRelationships("ECR Affected Item");
                Item relationshipsCollection = this.getRelationships("ECR Affected Item");

                int relCount = relationshipsCollection.getItemCount();
                List<string> associatedItems_list = new List<string>();
                for (int index = 0; index < relCount; index++)
                {
                    Item relationship = relationshipsCollection.getItemByIndex(index);
                    Item Related_Item = relationship.getRelatedItem();
                    Affected_id = Related_Item.getProperty("new_item_id");

                    Item AssociatedItems = myInnovator.applyMethod("Get_Associated_Item", "<id>" + Affected_id + "</id>");
                    string AssociatedItems_results = AssociatedItems.getResult();
                    int AssociatedItems_results_count = AssociatedItems_results.Split(',').Length;
                    associatedItems_list = AssociatedItems_results.Split(',').ToList();

                    associatedItems_list.RemoveAll(x => string.IsNullOrEmpty(x)); // Remove Null or Empty String in List
                    foreach (string associated_id in associatedItems_list)
                    {
                        // Check ECR Associated Item already exist or not.
                        Item ecr = myInnovator.newItem("ECR", "get");
                        ecr.setAttribute("where", "ECR.is_current='1' and ECR.id = '" + ecr_id + "'");
                        Item relationship_for_part = myInnovator.newItem("ECR Associated Item", "get");
                        Item relitm = myInnovator.newItem("Associated_Item", "get");
                        relitm.setProperty("associated_id", associated_id);
                        relationship_for_part.setRelatedItem(relitm);
                        ecr.addRelationship(relationship_for_part);
                        ecr = ecr.apply();

                        if (ecr.getItemCount() < 1)
                        {
                            Item check_part = myInnovator.newItem("Part", "get");
                            check_part.setProperty("id", associated_id);
                            check_part.setPropertyCondition("id", "eq");
                            check_part = check_part.apply();
                            if (check_part.getItemCount() > 0)
                            {
                                rev = check_part.getProperty("major_rev", "");
                            }
                            
                            Item check_doc = myInnovator.newItem("Document", "get");
                            check_doc.setProperty("id", associated_id);
                            check_doc.setPropertyCondition("id", "eq");
                            check_doc = check_doc.apply();
                            if (check_doc.getItemCount() > 0)
                            {
                                rev = check_doc.getProperty("major_rev", "");
                            }

                            Item controlledItem = myInnovator.getItemById("ECR", ecr_id);
                            if (controlledItem != null)
                            {
                                Item AssociatedItem = myInnovator.newItem("Associated_Item", "add");
                                AssociatedItem.setProperty("associated_id", associated_id);
                                AssociatedItem.setProperty("associated_rev", rev);
                                AssociatedItem.setProperty("affected_id", Affected_id);

                                Item ECR_Associated_item = myInnovator.newItem("ECR Associated Item", "add");
                                ECR_Associated_item.setRelatedItem(AssociatedItem);
                                controlledItem.addRelationship(ECR_Associated_item);
                                controlledItem = controlledItem.apply();
                                if (controlledItem.isError())
                                {
                                    return myInnovator.newError("Error : Add Associated Item : " + controlledItem.getErrorDetail());
                                }
                            }
                        }
                    }
                }

                // Delete ECR Associated Item if there is no Affected Item related with it.
                // And also Delete Duplicate Item if User added Associated Item manually.
                HashSet<string> duplicate = new HashSet<string>();
                bool contain_duplicate = false;

                this.fetchRelationships("ECR Associated Item");
                Item ecr_associated_items = this.getRelationships("ECR Associated Item");
                for (int j = 0; j < ecr_associated_items.getItemCount(); j++)
                {
                    Item relationship_item = ecr_associated_items.getItemByIndex(j);
                    Item related_item = relationship_item.getRelatedItem();
                    string item_id = related_item.getProperty("associated_id");

                    if (!duplicate.Add(item_id))
                    {
                        contain_duplicate = true;
                    }

                    if (!associatedItems_list.Contains(item_id) || contain_duplicate)
                    {
                        relationship_item.setAction("delete");
                        relationship_item.apply();
                        related_item.setAction("delete");
                        related_item.apply();
                    }
                }
                return this;
            }
            catch (System.Exception ex)
            {
                result = myInnovator.newError(string.Format("SYSTEM EXCEPTION in method {0}<br>{1}", MethodName, ex.Message));
                ////result.setErrorCode("0");		//only use if this method is called from another method
            }
            finally
            {
                //**** REVOKE IDENTITY PRIVILEGES
                // revokeIdentityPrivileges();
                //*******************************
            }

            return result;
        }
        /// <summary>
        /// This section is to allow the current method to assume the privileges of a specific identity.
        /// For instance calling: grantIdentityPrivileges("Aras PLM"); will allow this method all of the
        /// rights of the Aras PLM identity.
        /// The revokeIdentityPrivileges() method will removed the privileges added for this method. It
        /// must be called from before the method exits. Most likely in the Finally of the main method code.
        /// If different identity privileges must be granted a second time within the same method, first use
        /// revokeIdentityPrivileges() to remove the first privileges before granting new ones.
        /// </summary>

        private string AssignedIdentity;

        /// <summary>
        /// If required, call this helper function to run this method as another Identity. 
        /// Ensure that you call the revokeIdentityPrivileges helper function in the 
        /// finally block to revoke the Identity privilege when this method completes.
        /// </summary>
        public void grantIdentityPrivileges(string assignedIdent)
        {
            if (!string.IsNullOrEmpty(AssignedIdentity))
            {
                revokeIdentityPrivileges();
            }

            Aras.Server.Security.Identity secIdentity = Aras.Server.Security.Identity.GetByName(assignedIdent);
            if (Aras.Server.Security.Permissions.GrantIdentity(secIdentity))
            {
                AssignedIdentity = assignedIdent;
            }
        }

        /// <summary>
        /// If you granted security privileges, then you need to ensure that you revoke the privilege.
        /// </summary>
        public void revokeIdentityPrivileges()
        {
            if (AssignedIdentity != null)
            {
                Aras.Server.Security.Permissions.RevokeIdentity(Aras.Server.Security.Identity.GetByName(AssignedIdentity));
                AssignedIdentity = null;
            }]]></method_code>
  <method_type>C#</method_type>
  <name>add_associated_items</name>
 </Item>
</AML>