﻿<AML>
 <Item type="Method" id="25AE2944F206472CBAD42B6D8D627C8C" action="add">
  <comments />
  <execution_allowed_to keyed_name="World" type="Identity">A73B655731924CD0B027E4F4D5FCC0A9</execution_allowed_to>
  <method_code><![CDATA[
	Boolean permsWasSet = false;
	Aras.Server.Security.Identity identity = null;
	String techDocPriveledgedPublisherId = "86F8574A13814269A1CEC0E012AEADB2";
	String identityList = Aras.Server.Security.Permissions.Current.IdentitiesList;

	if (identityList.IndexOf(techDocPriveledgedPublisherId, System.StringComparison.Ordinal) != -1)
	{
		identity = Aras.Server.Security.Identity.GetByName("Super User");
		permsWasSet = Aras.Server.Security.Permissions.GrantIdentity(identity);
	}

	try
	{
		Innovator inn = this.getInnovator();

		String blockId = this.getID();
		String byReference = this.getAttribute("by-reference", "internal");
		Boolean isRootBlockExternal = byReference == "external";

		String defaultLangCode = this.getInnovator().getI18NSessionContext().GetDefaultLanguageCode();
		String langCode = this.getAttribute("language", defaultLangCode);

		String blockStructureAml = String.Format(CultureInfo.CurrentCulture, @"
			<Item type='tp_Block' select='id,content,xml_schema,condition,config_id,name,is_current,major_rev,generation' action='GetItemRepeatConfig' language='{0}'>
				<xml_schema>
					<Item type='tp_XmlSchema' select='name,content,target_namespace' action='get'>
						<Relationships>
							<Item type='tp_XmlSchemaElement' action='get' select='name,content_generator,is_content_dynamic'/>
						</Relationships>
					</Item>
				</xml_schema>
				<Relationships>
					<Item type='tp_BlockReference' select='reference_id,ignore_all_versions,ignored_version_id,related_id(id,content,condition,config_id,name,is_current,major_rev,generation)' repeatTimes='0' repeatProp='related_id'></Item>
					<Item type='tp_ImageReference' select='reference_id,ignore_all_versions,ignored_version_id,related_id(id,src,is_current,major_rev,generation)'></Item>
					<Item type='tp_ItemReference' select='reference_id,ignore_all_versions,ignored_version_id,related_id'></Item>
					<Item type='tp_LinkReference' select='reference_id,path,related_id,targetelement'></Item>
				</Relationships>
			</Item>", langCode);

		String externalLinksAml = String.Format(CultureInfo.CurrentCulture, @"
			<Item type='tp_Block' action='get'>
				<Relationships>
					<Item type='tp_LinkReference' action='get'>
						<related_id>{0}</related_id>
					</Item>
				</Relationships>
			</Item>", blockId);

		Item rootBlockItem = inn.newItem();
		rootBlockItem.loadAML(blockStructureAml);
		rootBlockItem.setID(blockId);
		rootBlockItem = rootBlockItem.apply();

		if(rootBlockItem.isError())// something's wrong. There is no element with such blockId or the user does not have permissions to get it
		{
			return inn.newError(rootBlockItem.getErrorString());
		}

		Item externalLinks = inn.newItem();
		externalLinks.loadAML(externalLinksAml);
		externalLinks = externalLinks.apply();

		Item xmlSchema = rootBlockItem.getPropertyItem("xml_schema");

		StringBuilder docContent = new StringBuilder();

		XmlWriterSettings settings = new XmlWriterSettings();
		settings.Indent = false;
		settings.OmitXmlDeclaration = true;

		using (XmlWriter xw = XmlWriter.Create(docContent, settings))
		{
			xw.WriteStartElement("aras", "document", "http://aras.com/ArasTechDoc");
			{
				xw.WriteStartAttribute("xmlns");
				xw.WriteValue(xmlSchema.getProperty("target_namespace"));
				xw.WriteEndAttribute();

				xw.WriteStartAttribute("schemaid");
				xw.WriteValue(xmlSchema.getID());
				xw.WriteEndAttribute();

				xw.WriteStartElement("aras", "content", "http://aras.com/ArasTechDoc");
				{
					AppendBlock(xw, langCode, rootBlockItem, null, byReference);
				}
				xw.WriteEndElement();

				// custom content generation section
				Item elementRelationships = xmlSchema.getRelationships("tp_XmlSchemaElement");
				int elementsCount = elementRelationships.getItemCount();
				List<String> dynamicElementNames = new List<String>();
				Dictionary<String, XmlNode> customContentNodes = new Dictionary<String, XmlNode>();
				Dictionary<String, XmlNode> customContentReferences = new Dictionary<String, XmlNode>();

				for (int i = 0; i < elementsCount; i++)
				{
					Item currentRelationship = elementRelationships.getItemByIndex(i);
					String contentGeneratorId = currentRelationship.getProperty("content_generator");

					if (!String.IsNullOrEmpty(contentGeneratorId) && currentRelationship.getProperty("is_content_dynamic") == "1")
					{
						dynamicElementNames.Add(currentRelationship.getProperty("name"));
					}
				}

				// if dynamic elements present in schema, then
				if (dynamicElementNames.Count > 0)
				{
					StringBuilder contentString = new StringBuilder();
					using (XmlWriter writer = XmlWriter.Create(contentString))
					{
						writer.WriteStartElement("aras", "document", "http://aras.com/ArasTechDoc");
						{
							writer.WriteStartAttribute("xmlns");
							writer.WriteValue(xmlSchema.getProperty("target_namespace"));
							writer.WriteEndAttribute();

							writer.WriteRaw(rootBlockItem.getProperty("content"));
						}
						writer.WriteEndElement();
					}

					XmlDocument documentContent = new XmlDocument();
					documentContent.LoadXml(contentString.ToString());
					List<XmlNode> nodeList = SearchDynamicElements(dynamicElementNames, documentContent.DocumentElement, new List<XmlNode>());

					// if in documentContent there are dynamic elements, then generate content for them
					if (nodeList.Count > 0)
					{
						Aras.TDF.Base.SchemaElementFactory factory = new Aras.TDF.Base.SchemaElementFactory(inn, xmlSchema.getID());
						factory.CustomContentEnabled = true;
						factory.DefaultLanguageCode = langCode;

						foreach (XmlNode elementNode in nodeList)
						{
							Aras.TDF.Base.DocumentSchemaNode schemaNode = factory.ParseElement(elementNode);

							factory.SearchReferenceNodesRecursively(schemaNode, customContentReferences);
							factory.SearchGeneratedContentNodesRecursively(schemaNode, customContentNodes);
						}
					}
				}

				xw.WriteStartElement("aras", "references", "http://aras.com/ArasTechDoc");
				{
					// Use // in xpath because we need to get references from all levels down
					Item blockReferences = rootBlockItem.getItemsByXPath("//Relationships/Item[@type='tp_BlockReference']");
					HashSet<String> existingReferences = new HashSet<String>();

					for (int referenceIndex = 0; referenceIndex < blockReferences.getItemCount(); referenceIndex++)
					{
						Item reference = blockReferences.getItemByIndex(referenceIndex);

						if (!IsReferenceDuplicate(reference, existingReferences))
						{
							AppendBlock(xw, langCode, reference.getRelatedItem(), reference, "external");
							customContentReferences.Remove(reference.getProperty("reference_id"));
						}
					}

					if (isRootBlockExternal) {
						AppendBlock(xw, langCode, rootBlockItem, rootBlockItem, "external");
						customContentReferences.Remove(rootBlockItem.getProperty("reference_id"));
					}

					Item graphicReferences = rootBlockItem.getItemsByXPath("//Relationships/Item[@type='tp_ImageReference']");
					for (int referenceIndex = 0; referenceIndex < graphicReferences.getItemCount(); referenceIndex++)
					{
						Item reference = graphicReferences.getItemByIndex(referenceIndex);

						if (!IsReferenceDuplicate(reference, existingReferences))
						{
							AppendImage(xw, reference);
							customContentReferences.Remove(reference.getProperty("reference_id"));
						}
					}

					Item itemReferences = rootBlockItem.getItemsByXPath("//Relationships/Item[@type='tp_ItemReference']");
					for (int referenceIndex = 0; referenceIndex < itemReferences.getItemCount(); referenceIndex++)
					{
						Item reference = itemReferences.getItemByIndex(referenceIndex);

						if (!IsReferenceDuplicate(reference, existingReferences))
						{
							AppendItem(xw, reference);
							customContentReferences.Remove(reference.getProperty("reference_id"));
						}
					}

					Item linkReferences = rootBlockItem.getItemsByXPath("//Relationships/Item[@type='tp_LinkReference']");
					for (int referenceIndex = 0; referenceIndex < linkReferences.getItemCount(); referenceIndex++)
					{
						Item reference = linkReferences.getItemByIndex(referenceIndex);

						AppendLink(xw, reference);
						customContentReferences.Remove(reference.getProperty("reference_id"));
					}

					foreach (XmlNode referenceNode in customContentReferences.Values)
					{
						referenceNode.WriteTo(xw);
					}
				}
				xw.WriteEndElement();

				xw.WriteStartElement("aras", "externalLinks", "http://aras.com/ArasTechDoc");
				{
					for (int i = 0; i < externalLinks.getItemCount(); i++)
					{
						Item sourceBlock = externalLinks.getItemByIndex(i);
						Item linkReference = sourceBlock.getRelationships("tp_LinkReference").getItemByIndex(0);

						AppendExternalLink(xw, sourceBlock, linkReference);
					}
				}
				xw.WriteEndElement();

				// append generatedContent if dynamic elements p
				xw.WriteStartElement("aras", "generatedContent", "http://aras.com/ArasTechDoc");
				{
					foreach (XmlNode elementNode in customContentNodes.Values)
					{
						elementNode.WriteTo(xw);
					}
				}
			}
			xw.WriteEndElement();
		}

		return inn.newResult(docContent.ToString());
	}
	finally
	{
		if (permsWasSet)
		{
			Aras.Server.Security.Permissions.RevokeIdentity(identity);
		}
	}
}

		static void AppendImage(XmlWriter xw, Item imageReference)
		{
			Item imageItem = imageReference.getRelatedItem();
			Boolean hasReferenceItem = imageItem != null;

			xw.WriteStartElement("image", "http://aras.com/ArasTechDoc");
			{
				System.Web.Script.Serialization.JavaScriptSerializer jsSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
				Dictionary<string, string> referenceProperties = new Dictionary<string, string>();

				if (hasReferenceItem)
				{
					String isCurrent = imageItem.getProperty("is_current");
					String latestVersionId = "";

					if (isCurrent == "0")
					{
						Item latestItemVersion = imageItem.newItem(imageItem.getType(), "getItemLastVersion");
						latestItemVersion.setID(imageItem.getID());
						latestItemVersion = latestItemVersion.apply();

						if (!latestItemVersion.isError()) {
							latestVersionId = latestItemVersion.getID();
						}
					}

					referenceProperties.Add("isCurrent", imageItem.getProperty("is_current"));
					referenceProperties.Add("majorVersion", imageItem.getProperty("major_rev"));
					referenceProperties.Add("minorVersion", imageItem.getProperty("generation"));
					referenceProperties.Add("latestVersionId", latestVersionId);

					if (imageItem.getAttribute("discover_only") == "1")
					{
						xw.WriteAttributeString("isBlocked", "true");
					}
					else
					{
						xw.WriteAttributeString("src", imageItem.getProperty("src"));
					}
					xw.WriteAttributeString("imageId", imageItem.getID());
				}
				else
				{
					if (imageReference.getPropertyAttribute("related_id", "is_null") == "0"){
						xw.WriteAttributeString("isBlocked", "true");
						String blockUid = Guid.NewGuid().ToString().Replace("-", "").ToUpper(CultureInfo.CurrentCulture);
						xw.WriteAttributeString("imageId", blockUid);
					}
				}

				referenceProperties.Add("referenceId", imageReference.getID());
				referenceProperties.Add("ignoredVersionId", imageReference.getProperty("ignored_version_id"));
				referenceProperties.Add("ignoreAllVersions", imageReference.getProperty("ignore_all_versions"));

				// add required attributes
				xw.WriteAttributeString("ref-id", imageReference.getProperty("reference_id"));
				xw.WriteAttributeString("referenceProperties", jsSerializer.Serialize(referenceProperties));
			}

			xw.WriteEndElement();
		}

		static void AppendLink(XmlWriter xw, Item itemReference)
		{
			xw.WriteStartElement("link", "http://aras.com/ArasTechDoc");
			{
				xw.WriteAttributeString("ref-id", itemReference.getProperty("reference_id"));
				xw.WriteAttributeString("linkId", itemReference.getID());
				xw.WriteAttributeString("path", itemReference.getProperty("path"));
				xw.WriteAttributeString("targetBlock", itemReference.getProperty("related_id"));
				xw.WriteAttributeString("targetElement", itemReference.getProperty("targetelement"));
			}
			xw.WriteEndElement();
		}

		static void AppendExternalLink(XmlWriter xw, Item sourceBlock, Item itemReference)
		{
			xw.WriteStartElement("aras", "link", "http://aras.com/ArasTechDoc");
			{
				xw.WriteAttributeString("linkId", itemReference.getID());
				xw.WriteAttributeString("sourceBlock", itemReference.getProperty("source_id"));
				xw.WriteAttributeString("blockName", sourceBlock.getProperty("keyed_name"));
				xw.WriteAttributeString("targetElement", itemReference.getProperty("targetelement"));
				xw.WriteAttributeString("path", itemReference.getProperty("path"));
			}
			xw.WriteEndElement();
		}

		static void AppendItem(XmlWriter xw, Item itemReference)
		{
			Item itemItem = itemReference.getRelatedItem();
			Boolean hasReferenceItem = itemItem != null;

			xw.WriteStartElement("item", "http://aras.com/ArasTechDoc");
			{
				System.Web.Script.Serialization.JavaScriptSerializer jsSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
				Dictionary<string, string> referenceProperties = new Dictionary<string, string>();

				if (hasReferenceItem)
				{
					String isCurrent = itemItem.getProperty("is_current");
					String latestVersionId = "";

					if (isCurrent == "0")
					{
						Item latestItemVersion = itemItem.newItem(itemItem.getType(), "getItemLastVersion");
						latestItemVersion.setID(itemItem.getID());
						latestItemVersion = latestItemVersion.apply();

						if (!latestItemVersion.isError())
						{
							latestVersionId = latestItemVersion.getID();
						}
					}

					referenceProperties.Add("isCurrent", itemItem.getProperty("is_current"));
					referenceProperties.Add("majorVersion", itemItem.getProperty("major_rev"));
					referenceProperties.Add("minorVersion", itemItem.getProperty("generation"));
					referenceProperties.Add("latestVersionId", latestVersionId);
				}

				referenceProperties.Add("referenceId", itemReference.getID());
				referenceProperties.Add("ignoredVersionId", itemReference.getProperty("ignored_version_id"));
				referenceProperties.Add("ignoreAllVersions", itemReference.getProperty("ignore_all_versions"));

				// add required attributes
				xw.WriteAttributeString("ref-id", itemReference.getProperty("reference_id"));
				xw.WriteAttributeString("referenceProperties", jsSerializer.Serialize(referenceProperties));

				if (hasReferenceItem)
				{
					if (itemItem.getAttribute("discover_only") == "1")
					{
						XmlNode itemEmptyNode = itemItem.node.CloneNode(false);

						xw.WriteAttributeString("isBlocked", "true");
						itemEmptyNode.WriteTo(xw);
					}
					else
					{
						itemItem.node.WriteTo(xw);
					}
				}

				if (!hasReferenceItem && itemReference.getPropertyAttribute("related_id", "is_null") == "0")
				{
					xw.WriteAttributeString("isBlocked", "true");
				}
			}

			xw.WriteEndElement();
		}

		static void AppendBlock(XmlWriter xw, String langCode, Item blockItem, Item referenceItem, String byReference)
		{
			Boolean isExternalBlock = byReference == "external";

			xw.WriteStartElement("aras", "block", "http://aras.com/ArasTechDoc");
			{
				if (isExternalBlock && blockItem == null)
				{
					String blockId = referenceItem.getProperty("reference_id");
					String blockUid = Guid.NewGuid().ToString().Replace("-", "").ToUpper(CultureInfo.CurrentCulture);

					xw.WriteAttributeString("ref-id", blockId);
					xw.WriteAttributeString("by-reference", byReference);
					xw.WriteAttributeString("aras", "id", null, blockUid);
					xw.WriteAttributeString("isBlocked", "true");
				}
				else if (isExternalBlock && referenceItem == null)
				{
					String blockId = blockItem.getID();
					String blockUid = Guid.NewGuid().ToString().Replace("-", "").ToUpper(CultureInfo.CurrentCulture);

					xw.WriteAttributeString("ref-id", blockId);
					xw.WriteAttributeString("by-reference", byReference);
					xw.WriteAttributeString("aras", "id", null, blockUid);
					blockItem.setProperty("reference_id", blockId);
				}
				else
				{
					String blockId = blockItem.getID();
					Boolean isBlocked = blockItem.getAttribute("discover_only") == "1";
					String conditionValue = blockItem.getProperty("condition", "");

					if (!String.IsNullOrEmpty(conditionValue))
					{
						xw.WriteAttributeString("aras", "condition", null, conditionValue);
					}

					xw.WriteAttributeString("blockId", blockId);
					xw.WriteAttributeString("by-reference", byReference);
					xw.WriteAttributeString("aras", "id", null, blockId);

					if (isExternalBlock)
					{
						System.Web.Script.Serialization.JavaScriptSerializer jsSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
						Dictionary<string, string> blockProperties = new Dictionary<string, string>();
						Dictionary<string, string> referenceProperties = new Dictionary<string, string>();
						String isCurrent = isBlocked ? "1" : blockItem.getProperty("is_current");
						String blockName = blockItem.getProperty("name");

						blockProperties.Add("name", !String.IsNullOrEmpty(blockName) ? blockName : blockId);
						referenceProperties.Add("referenceId", referenceItem.getID());
						referenceProperties.Add("isCurrent", isCurrent);

						if (!isBlocked)
						{
							String latestVersionId = String.Empty;

							if (isCurrent == "0")
							{
								Item latestBlockVersion = blockItem.newItem("tp_Block", "getItemLastVersion");
								latestBlockVersion.setID(blockId);
								latestBlockVersion = latestBlockVersion.apply();

								if (!latestBlockVersion.isError())
								{
									latestVersionId = latestBlockVersion.getID();
								}
							}

							referenceProperties.Add("majorVersion", blockItem.getProperty("major_rev"));
							referenceProperties.Add("minorVersion", blockItem.getProperty("generation"));
							referenceProperties.Add("latestVersionId", latestVersionId);
							referenceProperties.Add("ignoredVersionId", referenceItem.getProperty("ignored_version_id"));
							referenceProperties.Add("ignoreAllVersions", referenceItem.getProperty("ignore_all_versions"));
						}

						// add required attributes
						xw.WriteAttributeString("ref-id", referenceItem.getProperty("reference_id"));
						xw.WriteAttributeString("blockProperties", jsSerializer.Serialize(blockProperties));
						xw.WriteAttributeString("referenceProperties", jsSerializer.Serialize(referenceProperties));
					}

					if (isBlocked)
					{
						xw.WriteAttributeString("isBlocked", "true");
					}
					else
					{
						xw.WriteRaw(blockItem.getProperty("content", "", langCode));
					}
				}
			}
			xw.WriteEndElement();
		}

		List<XmlNode> SearchDynamicElements(List<String> compareList, XmlNode targetNode, List<XmlNode> targetList)
		{
			if (targetList == null)
			{
				targetList = new List<XmlNode>();
			}

			if (targetNode != null)
			{
				if (compareList.Contains(targetNode.Name))
				{
					targetList.Add(targetNode);
				}
				else if (targetNode.HasChildNodes)
				{
					foreach (XmlNode childNode in targetNode.ChildNodes)
					{
						if (childNode.NodeType == XmlNodeType.Element)
						{
							SearchDynamicElements(compareList, childNode, targetList);
						}
					}
				}
			}

			return targetList;
		}

		static Boolean IsReferenceDuplicate(Item referenceItem, HashSet<String> existingIds)
		{
			if (referenceItem != null)
			{
				String referenceId = referenceItem.getProperty("reference_id");

				if (!existingIds.Contains(referenceId))
				{
					existingIds.Add(referenceId);
				}
				else {
					return true;
				}
			}

			return false;
]]></method_code>
  <method_type>C#</method_type>
  <name>tp_DocumentGet</name>
 </Item>
</AML>