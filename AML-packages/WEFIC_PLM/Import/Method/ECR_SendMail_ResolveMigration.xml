﻿<AML>
 <Item type="Method" id="727C476830FF48F78568E2F156CA8AAC" action="add">
  <comments />
  <execution_allowed_to keyed_name="World" type="Identity">A73B655731924CD0B027E4F4D5FCC0A9</execution_allowed_to>
  <method_code><![CDATA[            /* *****************************************************************************
            Created By:		MyoThet
            Created On:		2020-08-27
            Description:
                Send Mail from Resolve Migration action
                to fix #193

            Hooks:
                called from: SG_Migration_Resolve
                params: ItemNumber, When, Type, ID

            Revisions:
                Rev Date        Modified By     Description
                2020-08-27      MyoThet         Initial Creation
            ***************************************************************************** */

            //WARNING: DO NOT FORGET TO DISABLE THIS IN A PRODUCTION ENVIRONMENT!!!
            //if (System.Diagnostics.Debugger.Launch()) System.Diagnostics.Debugger.Break();

            string MethodName = "ECR_SendMail_ResolveMigration";

            Innovator myInnovator = getInnovator();
            Item result = this; // default return value.

            DataAccessLayer dataAccessLayer = new DataAccessLayer(myInnovator);
            BusinessLogic businessLogic = new BusinessLogic(dataAccessLayer)
            {
                ControlledItem = this
            };

            bool status = businessLogic.SendMailResolveMigration(CCO);

            return status ? result : myInnovator.newError(MethodName + " FAILED");
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public class BusinessLogic
        {
            private readonly IDataAccessLayer dataAccessLayer;
            public Item ControlledItem { get; set; }

            public BusinessLogic(IDataAccessLayer dataAccessLayer)
            {
                this.dataAccessLayer = dataAccessLayer;
            }

            internal bool SendMailResolveMigration(Aras.Server.Core.CallContext CCO)
            {
                string controlledItemType = ControlledItem.getProperty("Type", "");
                string controlledItemId = ControlledItem.getProperty("ID", "");
                string controlledItemKeyedName = ControlledItem.getProperty("ItemNumber", "");

                // from mail address - authenticated address of SMTP Email configuration at IIS Server
                Item adminUser = dataAccessLayer.GetUserById("30B991F927274FA3829655F50C99472E"); // Innovator Admin
                string authenticatedEmailAddress = adminUser.getProperty("email", "aras@solidplm.com");

                // from name - release_by of ECR
                Item releasedECR = dataAccessLayer.GetReleasedECR(controlledItemType, controlledItemId);
                string releaseByIdentity = releasedECR.getProperty("release_by", "");
                string fromUserName = CCO.Utilities.GetIdentityNameByIdentityId(releaseByIdentity);

                // Must send notification to the creator of the item
                List<string> mailList = new List<string>();
                string modifiedByUser = releasedECR.getProperty("modified_by_id", "");
                Item creator = dataAccessLayer.GetUserById(modifiedByUser);
                string creator_email = creator.getProperty("email", "");
                mailList.Add(creator_email);

                // send notification to super user - to check resolve migration success
                Item superUser = dataAccessLayer.GetUserById("AD30A6D8D3B642F5A2AFED1A4B02BEFA");
                string superUserEmail = superUser.getProperty("email", "myothet@solidplm.com");
                mailList.Add(superUserEmail);

                string strBaseUrlWithoutSalt = CCO.Utilities.GetVarValue("baseUrlWithoutSalt", "http://192.168.1.5/InnovatorServer11SP7/default.aspx");
                string startItem = "?StartItem=" + controlledItemType + ":" + controlledItemId;
                string controlledItemUrl = strBaseUrlWithoutSalt + startItem;
                string controlledItemLink = "<a href='" + controlledItemUrl + "'>" + controlledItemKeyedName + "</a>";

                StringBuilder sbBody = new StringBuilder();
                sbBody.Append("Dear Sir/Madam,");
                sbBody.Append("<br/><br/>");
                sbBody.Append(controlledItemKeyedName + " have been Released.");
                sbBody.Append("<br/><br/>");
                sbBody.Append("Please find " + controlledItemLink + " for details.");
                sbBody.Append("<br/><br/>");
                sbBody.Append("Best Regards,<br/>");
                sbBody.Append("Aras PLM");
                string emBody = sbBody.ToString();

                Item rsEmailNotifications = dataAccessLayer.GetEmailNotifications(controlledItemId);
                int count = rsEmailNotifications.getItemCount();

                for (int i = 0; i < count; i++)
                {
                    Item rsItem = rsEmailNotifications.getItemByIndex(i);
                    string k_email = rsItem.getProperty("k_email", "");
                    string k_identity = rsItem.getProperty("k_identity", "");

                    // Prevent sending mail to outside users - Identity cannot be empty
                    if (!string.IsNullOrEmpty(k_email) && !string.IsNullOrEmpty(k_identity))
                    {
                        mailList.Add(k_email);
                    }
                }

                List<string> distinct_user_mail = mailList.Distinct().ToList();
                foreach (string user_mail in distinct_user_mail)
                {
                    System.Net.Mail.MailAddress fromMailAddress = new System.Net.Mail.MailAddress(authenticatedEmailAddress, fromUserName);
                    System.Net.Mail.MailAddress toMailAddress = new System.Net.Mail.MailAddress(user_mail);
                    using (System.Net.Mail.MailMessage MyMessage = new System.Net.Mail.MailMessage(fromMailAddress, toMailAddress))
                    {
                        MyMessage.Subject = controlledItemKeyedName + " have been Released";
                        MyMessage.IsBodyHtml = true;
                        MyMessage.Body = emBody;

                        CCO.Email.SetupSmtpMailServerAndSend(MyMessage);
                    }

                    System.Threading.Thread.Sleep(2000);
                }

                return true;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
        public interface IDataAccessLayer
        {
            Item GetReleasedECR(string controlledItemType, string controlledItemId);
            Item GetEmailNotifications(string controlledItemId);
            Item GetUserById(string userId);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
        public class DataAccessLayer : IDataAccessLayer
        {
            private readonly Innovator myInnovator;

            internal DataAccessLayer(Innovator myInnovator)
            {
                this.myInnovator = myInnovator;
            }

            public Item GetUserById(string userId)
            {
                Item item = myInnovator.newItem("User", "get");
                item.setAttribute("select", "first_name, last_name, email");
                item.setID(userId);
                return item.apply();
            }

            public Item GetEmailNotifications(string controlledItemId)
            {
                StringBuilder sbEmail = new StringBuilder("");
                sbEmail.Append(" select k_identity, k_email ");
                sbEmail.Append(" from ECR_EMAIL");
                sbEmail.Append(" where source_id = '" + controlledItemId + "'");
                string sqlEmail = sbEmail.ToString();

                return myInnovator.applySQL(sqlEmail);
            }

            public Item GetReleasedECR(string controlledItemType, string controlledItemId)
            {
                Item item = myInnovator.newItem(controlledItemType, "get");
                item.setAttribute("select", "release_by, modified_by_id");
                item.setID(controlledItemId);
                return item.apply();
            }
]]></method_code>
  <method_type>C#</method_type>
  <name>ECR_SendMail_ResolveMigration</name>
 </Item>
</AML>