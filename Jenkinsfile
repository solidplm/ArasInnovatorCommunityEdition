properties([
	pipelineTriggers([]),
	disableConcurrentBuilds(),
])

def emailExtRecipientProviders
def EmailSuccess
def EmailFailure
def EmailRecipients

try
{
	node('consulting') {
		try {
			timestamps {
				emailExtRecipientProviders = (env["EmailExtRecipientProviders"] == null) ? '' : env["EmailExtRecipientProviders"]
				EmailSuccess = (env["EmailSuccess"] == null) ? '' : env["EmailSuccess"]
				EmailFailure = (env["EmailFailure"] == null) ? '' : env["EmailFailure"]
				EmailRecipients = EmailSuccess
				def fullBuildPath = pwd()

				dir (fullBuildPath) { 
					stage('Checkout') {
					def referenceRepository = (env.sandboxRootDir == null) ? [] : [[$class: 'CloneOption', reference: "${env.sandboxRootDir}\\customerrepositorytemplate.git"]]

						checkout([
							$class: 'GitSCM',
							branches: scm.branches,
							extensions: scm.extensions + referenceRepository,
							userRemoteConfigs: scm.userRemoteConfigs
						])
					}

					def credentialBindings = []
					def credentialList = ['MSSQL.Innovator.Password', 'MSSQL.Innovator.Regular.Password', 'MSSQL.SA.Password']
					for (i = 0; i < credentialList.size(); i++) {
						def doAdd = false
						def credentialId = credentialList[i]
						def credential = string(credentialsId: credentialId, variable: credentialId)
						try {
							withCredentials([credential]) {
							
								doAdd = (env[credentialId] != null) && env[credentialId] != ''
							}
						}
						catch (org.jenkinsci.plugins.credentialsbinding.impl.CredentialNotFoundException e) {
							doAdd = false
						}
						finally {
							if (doAdd) {
								credentialBindings.add(credential)
							}
						}
					}
					withCredentials(credentialBindings) {
						def nantParams = ''
						nantParams += (env["LicenseType"] == null) ? ' -D:Innovator.License.Type=Unlimited' : " -D:Innovator.License.Type=" + env["LicenseType"]
						nantParams += (env["LicenseActivationKey"] == null) ? '' : " -D:Innovator.Activation.Key=" + env["LicenseActivationKey"]
						nantParams += (env["LicenseKey"] == null) ? '' : " -D:Innovator.License.Key=" + env["LicenseKey"]
						nantParams += (env["MSSQL.Innovator.Password"] == null) ? ' -D:MSSQL.Innovator.Password=innovator' : " -D:MSSQL.Innovator.Password=" + env["MSSQL.Innovator.Password"]
						nantParams += (env["MSSQL.Innovator.Regular.Password"] == null) ? ' -D:MSSQL.Innovator.Regular.Password=innovator' : " -D:MSSQL.Innovator.Regular.Password=" + env["MSSQL.Innovator.Regular.Password"]
						nantParams += (env["MSSQL.SA.Password"] == null) ? '' : " -D:MSSQL.SA.Password=" + env["MSSQL.SA.Password"]
						nantParams += (env["MSSQL.Server"] == null) ? '' : " -D:MSSQL.Server=" + env["MSSQL.Server"]
						nantParams += (env["Path.To.CodeTree.Zip"] == null) ? '' : " -D:Path.To.CodeTree.Zip=" + env["Path.To.CodeTree.Zip"]
						nantParams += (env["Path.To.DB.Bak"] == null) ? '' : " -D:Path.To.DB.Bak=" + env["Path.To.DB.Bak"]
						nantParams += (env["Url.Of.Deployment.Server"] == null) ? '' : " -D:Url.Of.Deployment.Server=" + env["Url.Of.Deployment.Server"]
						nantParams += (env["MachineSpecific.Includes.Folder.Path"] == null) ? '' : " -D:MachineSpecific.Includes.Folder.Path=" + env["MachineSpecific.Includes.Folder.Path"]

						//CHANGE_TARGET: For a multibranch project corresponding to some kind of change request, this will be set to the target or base branch to which the change could be merged.
						def branchName = (env.CHANGE_TARGET) ? env.CHANGE_TARGET : env.BRANCH_NAME
						nantParams += " -D:Branch.Name=" + branchName
					
						def nantTargetsToRun = 'ContinuousIntegration'
						nantParams += ' ' + nantTargetsToRun

						stage('ContinuousIntegration') {
							bat '"AutomatedProcedures\\tools\\nant-0.92\\bin\\nant.exe" -buildfile:AutomatedProcedures\\NantScript.xml' + nantParams
						}
					}
				}
				currentBuild.result = 'SUCCESS'
			}
		}
		finally {
			stage('Cleanup Jenkins Workspace') {
				cleanWs notFailBuild: true, patterns: [[pattern: 'AutomatedProceduresOutput', type: 'EXCLUDE']]
			}
		}
	}
}
finally {
	stage('Send emails') {
		if (currentBuild.result == null) {
			currentBuild.result = 'FAILURE'

			def recipientProviders = []
			if (emailExtRecipientProviders != null && emailExtRecipientProviders != '') {
				def providerNames = emailExtRecipientProviders.split()
				for (i = 0; i < providerNames.size(); i++) {
						recipientProviders.add([$class: providerNames[i]])
				}
			}
			def providedRecipients = recipientProviders.isEmpty() ? '' : emailextrecipients(recipientProviders)
			EmailRecipients = "${EmailFailure} ${providedRecipients}".trim()
		}
		if (EmailRecipients != '') {
			emailext body: '$DEFAULT_CONTENT',subject: '[JENKINS] ' + '$DEFAULT_SUBJECT', to: EmailRecipients
		}
	}
}