@echo off

echo RunTests.bat
echo Target audience: QA team, Development team
echo Purpose: Run Integration tests.

SET PathToThisBatFileFolder=%~dp0
CALL "%PathToThisBatFileFolder%AutomatedProcedures\BatchUtilityScripts\GetMachineSpecificIncludes.bat
IF errorlevel 1 GOTO END

FOR /f "delims= " %%i in ('wmic computersystem get name') DO FOR /f "delims=" %%t in ("%%i") DO SET LocalMachineName=%%t
FOR /f "delims=" %%i in ('git rev-parse --abbrev-ref HEAD') DO SET BranchName=%%i

"%PathToThisBatFileFolder%AutomatedProcedures\tools\nant-0.92\bin\nant.exe" ^
	"/f:%PathToThisBatFileFolder%AutomatedProcedures\NantScript.xml" ^
	SetupParameters.For.Developer.Environment ^
	RunIntegrationTests ^
	-D:Local.Machine.Name=%LocalMachineName% ^
	-D:Branch.Name="%BranchName%" ^
	-D:MachineSpecific.Includes.Folder.Path=%MachineSpecificIncludesPath% ^
	-D:Is.MachineSpecific.Includes.Mandatory=true

:END
if not errorlevel 1 (
	powershell write-host -foregroundcolor green "SUCCESS!!!"
) else (
	powershell write-host -foregroundcolor red "FAILURE!!!"
)

pause