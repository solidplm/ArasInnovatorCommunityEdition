USE master;
IF (EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE ('[' + name + ']' = 'ArasInnovatorCommunityEdition' OR name = 'ArasInnovatorCommunityEdition')))
BEGIN
	ALTER DATABASE [ArasInnovatorCommunityEdition] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	DROP DATABASE [ArasInnovatorCommunityEdition]
END
RESTORE DATABASE [ArasInnovatorCommunityEdition]
FROM DISK = 'C:\SolidCAD\Development\_CleanDatabaseBackups\Temp Database\ArasInnovatorCommunityEditionClean.bak'
WITH REPLACE, 
MOVE 'ArasInnovatorCommunityEdition' TO 'C:\SolidCad\Development\_CleanDatabaseBackups\ArasInnovatorCommunityEdition.mdf',
MOVE 'ArasInnovatorCommunityEdition_log' TO 'C:\SolidCad\Development\_CleanDatabaseBackups\ArasInnovatorCommunityEdition.ldf'

USE [ArasInnovatorCommunityEdition];
GO

sp_change_users_login 'Auto_Fix','innovator';
GO

sp_change_users_login 'Auto_Fix','innovator_regular';
GO