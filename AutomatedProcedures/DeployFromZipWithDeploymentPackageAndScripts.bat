@echo off

echo DeployFromZipArchiveWithPackageAndScripts.bat:
echo Target audience: Deployment engineer, QA team;
echo Purpose: Deploy upgrade to the target server from zip with diff

SET NAntTargetsToRun=Deploy.Package
SET PathToThisBatFileFolder=%~dp0

CALL:ConfirmUpdate "Confirm that you sure you want to update Innovator code tree? [Y] Yes [N] No" SkipInnovatorCodeTreeUpgrade -D:Skip.Innovator.CodeTree.Upgrade
CALL:ConfirmUpdate "Confirm that you sure you want to update Innovator database? [Y] Yes [N] No" SkipInnovatorDBUpgrade -D:Skip.Innovator.DB.Upgrade
CALL:ConfirmUpdate "Confirm that you sure you want to update Vault Server code tree? [Y] Yes [N] No" SkipVaultServerCodeTreeUpgrade -D:Skip.VaultServer.CodeTree.Upgrade
CALL:ConfirmUpdate "Confirm that you sure you want to update Agent Service code tree? [Y] Yes [N] No" SkipAgentServiceCodeTreeUpgrade -D:Skip.AgentService.CodeTree.Upgrade
CALL:ConfirmUpdate "Confirm that you sure you want to update Conversion Server code tree? [Y] Yes [N] No" SkipConversionServerCodeTreeUpgrade -D:Skip.ConversionServer.CodeTree.Upgrade

"%PathToThisBatFileFolder%tools\nant-0.92\bin\nant.exe" ^
	"/f:%PathToThisBatFileFolder%NantScript.xml" ^
	%NAntTargetsToRun% ^
	%SkipInnovatorCodeTreeUpgrade% ^
	%SkipInnovatorDBUpgrade% ^
	%SkipVaultServerCodeTreeUpgrade% ^
	%SkipAgentServiceCodeTreeUpgrade% ^
	%SkipConversionServerCodeTreeUpgrade% ^
	"-D:Path.To.Deployment.Package.Dir=%PathToThisBatFileFolder%.."

if not errorlevel 1 (
	powershell write-host -foregroundcolor green "EVERYTHING WAS DEPLOYED SUCCESSFULLY!!!"
)

pause
goto :eof

:ConfirmUpdate
CHOICE /T 15 /N /d N /M %1
IF errorlevel 2 (
	REM If selected 'no' - evaluate nant parameter to skip update
	SET %2="%3=yes"
	GOTO :eof
)
REM If selected 'yes' - set empty string, so nant will not skip update
IF errorlevel 1 SET %2=
GOTO :eof