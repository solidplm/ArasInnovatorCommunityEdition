// A pipeline script intended to perform deploy on a target environment via Jenkins
def buildAgentNodeLabel='master'
def mssqlInnovatorPasswordCredentialId = 'MSSQL.Innovator.Password'
def mssqlSaPasswordCredentialId = 'MSSQL.SA.Password'
def innovatorLicenseStringSecretTextId = 'Innovator.License.String'

if (params.CustomBuildAgentNodeLabel){
	buildAgentNodeLabel = params.CustomBuildAgentNodeLabel
}
if (params.CustomMssqlInnovatorPasswordCredentialId){
	mssqlInnovatorPasswordCredentialId = params.CustomMssqlInnovatorPasswordCredentialId
}
if (params.CustomMssqlSaPasswordCredentialId){
	mssqlSaPasswordCredentialId = params.CustomMssqlSaPasswordCredentialId
}
if (params.InnovatorLicenseStringSecretTextId){
	innovatorLicenseStringSecretTextId = params.InnovatorLicenseStringSecretTextId
}

def gitCheckoutTimeout = 60
def nantParams = ''

def emailExtRecipientProviders
def EmailSuccess
def EmailFailure
def EmailRecipients
try
{
	node(buildAgentNodeLabel) {
		buildAgentNodeLabel = env.NODE_NAME
		emailExtRecipientProviders = (env["EmailExtRecipientProviders"] == null) ? '' : env["EmailExtRecipientProviders"]
		EmailRecipients = (env["EmailSuccess"] == null) ? '' : env["EmailSuccess"]
		EmailFailure = (env["EmailFailure"] == null) ? '' : env["EmailFailure"]

		def credentialBindings = []
		def credentialList = [mssqlInnovatorPasswordCredentialId, mssqlSaPasswordCredentialId, innovatorLicenseStringSecretTextId]
		for (i = 0; i < credentialList.size(); i++) {
			def doAdd = false
			def credentialId = credentialList[i]
			def credential = string(credentialsId: credentialId, variable: credentialId)
			try {
				withCredentials([credential]) {
					doAdd = (env[credentialId] != null) && env[credentialId] != ''
				}
			}
			catch (org.jenkinsci.plugins.credentialsbinding.impl.CredentialNotFoundException e) {
				doAdd = false
			}
			finally {
				if (doAdd) {
					credentialBindings.add(credential)
				}
			}
		}

		stage('Checkout') {
			checkout scm
		}

		withCredentials(credentialBindings) {
			nantParams += (env["LicenseType"] == null) ? ' -D:Innovator.License.Type=Unlimited' : " -D:Innovator.License.Type=" + env["LicenseType"]
			nantParams += (env["LicenseActivationKey"] == null) ? '' : " -D:Innovator.Activation.Key=" + env["LicenseActivationKey"]
			nantParams += (env["LicenseKey"] == null) ? '' : " -D:Innovator.License.Key=" + env["LicenseKey"]
			nantParams += (env[mssqlInnovatorPasswordCredentialId] == null) ? ' -D:MSSQL.Innovator.Password=innovator' : ' -D:MSSQL.Innovator.Password="' + env[mssqlInnovatorPasswordCredentialId] +'"'
			nantParams += (env[mssqlSaPasswordCredentialId] == null) ? '' : ' -D:MSSQL.SA.Password="' + env[mssqlSaPasswordCredentialId] +'"'
			nantParams += (env["Path.To.Installed.Innovator"] == null) ? '' : ' -D:Path.To.Installed.Innovator="' + env["Path.To.Installed.Innovator"] +'"'
			nantParams += (env["Url.Of.Installed.Innovator"] == null) ? '' : ' -D:Url.Of.Installed.Innovator="' + env["Url.Of.Installed.Innovator"] +'"'
			nantParams += (env["MSSQL.Server"] == null) ? '' : ' -D:MSSQL.Server="' + env["MSSQL.Server"] +'"'
			nantParams += (env["MSSQL.Database.Name"] == null) ? '' : ' -D:MSSQL.Database.Name="' + env["MSSQL.Database.Name"] +'"'
			nantParams += (env["Agent.Service.Name"] == null) ? '' : ' -D:Agent.Service.Name="' + env["Agent.Service.Name"] +'"'
			nantParams += (env["Agent.Service.Host.Name"] == null) ? '' : ' -D:Agent.Service.Host.Name="' + env["Agent.Service.Host.Name"] +'"'
			nantParams += (env["Path.To.Sandbox.Directory"] == null) ? '' : ' -D:Path.To.Sandbox.Directory="' + env["Path.To.Sandbox.Directory"] + '"'
			if (env[innovatorLicenseStringSecretTextId] != null)
			{
				def innovatorLicenseStringSecretText = env[innovatorLicenseStringSecretTextId].replace('"', '\'')
				nantParams += ' -D:Innovator.License.String="' + innovatorLicenseStringSecretText +'"'
			}

			stage('Run.Pre.Deploy.Validations') {
				bat '"AutomatedProcedures\\tools\\nant-0.92\\bin\\nant.exe" -buildfile:AutomatedProcedures\\NantScript.xml' + nantParams + ' Run.Pre.Deploy.Validations'
			}

			stage('Get delta') {
				bat '"AutomatedProcedures\\tools\\nant-0.92\\bin\\nant.exe" -buildfile:AutomatedProcedures\\NantScript.xml' + nantParams + ' GetGitDiffBetweenCurrentHeadStateAndLastRelease'
			}

			stage('Upgrade.CodeTree') {
				bat '"AutomatedProcedures\\tools\\nant-0.92\\bin\\nant.exe" -buildfile:AutomatedProcedures\\NantScript.xml' + nantParams + ' Upgrade.CodeTree'
			}

			stage('Upgrade.DB') {
				bat '"AutomatedProcedures\\tools\\nant-0.92\\bin\\nant.exe" -buildfile:AutomatedProcedures\\NantScript.xml' + nantParams + ' Upgrade.DB'
			}
		}
	}

	currentBuild.result = 'SUCCESS'
}
finally {
	stage('Send emails') {
		if (currentBuild.result == null) {
			currentBuild.result = 'FAILURE'

			def recipientProviders = []
			if (emailExtRecipientProviders != null && emailExtRecipientProviders != '') {
				def providerNames = emailExtRecipientProviders.split()
				for (i = 0; i < providerNames.size(); i++) {
					recipientProviders.add([$class: providerNames[i]])
				}
			}
			def providedRecipients = recipientProviders.isEmpty() ? '' : emailextrecipients(recipientProviders)
			EmailRecipients = "${EmailFailure} ${providedRecipients}".trim()
		}
		if (EmailRecipients != '') {
			emailext body: '$DEFAULT_CONTENT',subject: '[JENKINS] ' + '$DEFAULT_SUBJECT', to: EmailRecipients
		}
	}
}