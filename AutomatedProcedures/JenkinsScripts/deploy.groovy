// A pipeline script intended to perform deploy on a target environment via Jenkins
def branchToDeploy
def buildSpecificName
def repositoryUrl
def userEmailForTag
def userNameForTag
def buildAgentNodeLabel='master'
def mssqlInnovatorPasswordCredentialId = 'MSSQL.Innovator.Password'
def mssqlInnovatorRegularPasswordCredentialId = 'MSSQL.Innovator.Regular.Password'
def mssqlSaPasswordCredentialId = 'MSSQL.SA.Password'
def innovatorLicenseStringSecretTextId = 'Innovator.License.String'

if (params.CustomBuildAgentNodeLabel){
	buildAgentNodeLabel = params.CustomBuildAgentNodeLabel
}
if (params.CustomMssqlInnovatorPasswordCredentialId){
	mssqlInnovatorPasswordCredentialId = params.CustomMssqlInnovatorPasswordCredentialId
}
if (params.CustomMssqlInnovatorRegularPasswordCredentialId){
	mssqlInnovatorRegularPasswordCredentialId = params.CustomMssqlInnovatorRegularPasswordCredentialId
}
if (params.CustomMssqlSaPasswordCredentialId){
	mssqlSaPasswordCredentialId = params.CustomMssqlSaPasswordCredentialId
}
if (params.InnovatorLicenseStringSecretTextId){
	innovatorLicenseStringSecretTextId = params.InnovatorLicenseStringSecretTextId
}

if (params.BranchName) {
	branchToDeploy = params.BranchName
} else {
	error "The BranchName parameter is mandatory for deployment jobs." + 
		"Go 'Configure'->'This project is parametrised' and add the BranchName string parameter"
}
if (params.NodeLabel) {
	deployAgentNodeLabel = params.NodeLabel
} else {
	deployAgentNodeLabel = branchToDeploy
}
if (params.RepositoryUrl) {
	repositoryUrl = params.RepositoryUrl
} else {
	error "The RepositoryUrl parameter is mandatory for deployment jobs." + 
		"Go 'Configure'->'This project is parametrised' and add the RepositoryUrl string parameter"
}

if (params.UserEmailForTag && params.UserNameForTag) {
	userEmailForTag = params.UserEmailForTag
	userNameForTag = params.UserNameForTag
} else {
	error "The UserEmailForTag and UserNameForTag parameters are mandatory for deployment jobs." +
		"They are used to create a git tag" 
		"Go 'Configure'->'This project is parametrised' and add these string parameters"
}

def gitCheckoutTimeout = 60
def nantParams = ''

def emailExtRecipientProviders
def EmailSuccess
def EmailFailure
def EmailRecipients
def innovatorDeploymentPackageArchiveName

try
{
	node(buildAgentNodeLabel) {
		buildAgentNodeLabel = env.NODE_NAME
		buildSpecificName = "${branchToDeploy}-${env.BUILD_NUMBER}"
		innovatorDeploymentPackageArchiveName = "${buildSpecificName}.zip"
		emailExtRecipientProviders = (env["EmailExtRecipientProviders"] == null) ? '' : env["EmailExtRecipientProviders"]
		EmailRecipients = (env["EmailSuccess"] == null) ? '' : env["EmailSuccess"]
		EmailFailure = (env["EmailFailure"] == null) ? '' : env["EmailFailure"]
		stage('Prepare Deployment Package') {
			checkout([
				$class: 'GitSCM',
				branches: [[name: "*/${branchToDeploy}"]],
				doGenerateSubmoduleConfigurations: false,
				extensions: [[$class: 'CloneOption', depth: 0, noTags: false, reference: '', shallow: false, timeout: gitCheckoutTimeout]],
				submoduleCfg: [],
				userRemoteConfigs: [[credentialsId: 'jenkins-user', url:repositoryUrl]]
			])

			nantParams += " -D:Deployment.Package.Archive.Name=${innovatorDeploymentPackageArchiveName}"

			bat "\"AutomatedProcedures\\tools\\nant-0.92\\bin\\nant.exe\" -buildfile:AutomatedProcedures\\NantScript.xml ${nantParams} CreateZipWithDeploymentPackageAndScripts"

			stash name: "InnovatorDeploymentPackage", includes:innovatorDeploymentPackageArchiveName
		}
	}
	node(deployAgentNodeLabel) {
		stage('Cleanup Jenkins Workspace') {
			cleanWs notFailBuild: true
		}
		def credentialBindings = []
		def credentialList = [mssqlInnovatorPasswordCredentialId, mssqlInnovatorRegularPasswordCredentialId, mssqlSaPasswordCredentialId, innovatorLicenseStringSecretTextId]
		for (i = 0; i < credentialList.size(); i++) {
			def doAdd = false
			def credentialId = credentialList[i]
			def credential = string(credentialsId: credentialId, variable: credentialId)
			try {
				withCredentials([credential]) {
					doAdd = (env[credentialId] != null) && env[credentialId] != ''
				}
			}
			catch (org.jenkinsci.plugins.credentialsbinding.impl.CredentialNotFoundException e) {
				doAdd = false
			}
			finally {
				if (doAdd) {
					credentialBindings.add(credential)
				}
			}
		}
		withCredentials(credentialBindings) {
			nantParams += (env["LicenseType"] == null) ? ' -D:Innovator.License.Type=Unlimited' : " -D:Innovator.License.Type=" + env["LicenseType"]
			nantParams += (env["LicenseActivationKey"] == null) ? '' : " -D:Innovator.Activation.Key=" + env["LicenseActivationKey"]
			nantParams += (env["LicenseKey"] == null) ? '' : " -D:Innovator.License.Key=" + env["LicenseKey"]
			nantParams += (env[mssqlInnovatorPasswordCredentialId] == null) ? ' -D:MSSQL.Innovator.Password=innovator' : ' -D:MSSQL.Innovator.Password="' + env[mssqlInnovatorPasswordCredentialId] +'"'
			nantParams += (env[mssqlInnovatorRegularPasswordCredentialId] == null) ? ' -D:MSSQL.Innovator.Regular.Password=innovator' : ' -D:MSSQL.Innovator.Regular.Password="' + env[mssqlInnovatorRegularPasswordCredentialId] +'"'
			nantParams += (env[mssqlSaPasswordCredentialId] == null) ? '' : ' -D:MSSQL.SA.Password="' + env[mssqlSaPasswordCredentialId] +'"'
			nantParams += (env["MSSQL.Server"] == null) ? '' : ' -D:MSSQL.Server="' + env["MSSQL.Server"] +'"'
			nantParams += (env["MSSQL.Database.Name"] == null) ? '' : ' -D:MSSQL.Database.Name="' + env["MSSQL.Database.Name"] +'"'
			nantParams += (env["Path.To.CodeTree.Zip"] == null) ? '' : ' -D:Path.To.CodeTree.Zip="' + env["Path.To.CodeTree.Zip"] +'"'
			nantParams += (env["Path.To.DB.Bak"] == null) ? '' : ' -D:Path.To.DB.Bak="' + env["Path.To.DB.Bak"] +'"'
			nantParams += (env["Path.To.Sandbox.Directory"] == null) ? '' : ' -D:Path.To.Sandbox.Directory="' + env["Path.To.Sandbox.Directory"] + '"'
			if (env[innovatorLicenseStringSecretTextId] != null)
			{
				def innovatorLicenseStringSecretText = env[innovatorLicenseStringSecretTextId].replace('"', '\'')
				nantParams += ' -D:Innovator.License.String="' + innovatorLicenseStringSecretText +'"'
			}

			nantParams += (env["Url.Of.Deployment.Server"] == null) ? '' : ' -D:Url.Of.Deployment.Server="' + env["Url.Of.Deployment.Server"] +'"'
			nantParams += (env["Port.Of.Deployment.Server"] == null) ? '' : ' -D:Port.Of.Deployment.Server="' + env["Port.Of.Deployment.Server"] +'"'
			nantParams += (env["IIS.Default.SiteName"] == null) ? '' : ' -D:IIS.Default.SiteName="' + env["IIS.Default.SiteName"] +'"'
			//based on the Name.Of.Innovator.Instance path to Innovator and url will be generated
			nantParams +=  " -D:Branch.Name=${branchToDeploy}"
			nantParams +=  " -D:Build.Number=${env.BUILD_NUMBER}"
			nantParams += " -D:Path.To.Deployment.Package.Dir=${pwd()}"

			stage('Setup Innovator & Deploy') {
				unstash name: "InnovatorDeploymentPackage"
				unzip zipFile: innovatorDeploymentPackageArchiveName
				bat '"AutomatedProcedures\\tools\\nant-0.92\\bin\\nant.exe" -buildfile:AutomatedProcedures\\NantScript.xml' + nantParams + ' Setup.Innovator.For.Deploy.Task'
				bat '"AutomatedProcedures\\tools\\nant-0.92\\bin\\nant.exe" -buildfile:AutomatedProcedures\\NantScript.xml' + nantParams + ' SetupParameters.For.Deploy.Task Run.Pre.Deploy.Validations Upgrade.CodeTree Upgrade.DB Print.Url.Of.Installed.Innovator'
			}
		}
	}
	node(buildAgentNodeLabel) {
		stage('Git tag') {
			sshagent(['jenkins-user']) {
				bat("git config user.email ${userEmailForTag}")
				bat("git config user.name \"${userNameForTag}\"")
				bat("git tag ${buildSpecificName} -a -m \"Jenkins ${buildSpecificName} build\"")
				bat("git push ${repositoryUrl} ${buildSpecificName}")
			}
		}
	}
	currentBuild.result = 'SUCCESS'
}
finally {
	stage('Send emails') {
		if (currentBuild.result == null) {
			currentBuild.result = 'FAILURE'

			def recipientProviders = []
			if (emailExtRecipientProviders != null && emailExtRecipientProviders != '') {
				def providerNames = emailExtRecipientProviders.split()
				for (i = 0; i < providerNames.size(); i++) {
						recipientProviders.add([$class: providerNames[i]])
				}
			}
			def providedRecipients = recipientProviders.isEmpty() ? '' : emailextrecipients(recipientProviders)
			EmailRecipients = "${EmailFailure} ${providedRecipients}".trim()
		}
		if (EmailRecipients != '') {
			emailext body: '$DEFAULT_CONTENT',subject: '[JENKINS] ' + '$DEFAULT_SUBJECT', to: EmailRecipients
		}
	}
}