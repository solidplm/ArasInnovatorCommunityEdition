@echo off

echo CreateZipWithDeploymentPackageAndScripts.bat:
echo Target audience: Deployment engineer;
echo Purpose: Create Zip archive with deployment script that can be used to upgrade the target server

SET NAntTargetsToRun=CreateZipWithDeploymentPackageAndScripts
SET PathToThisBatFileFolder=%~dp0

"%PathToThisBatFileFolder%tools\nant-0.92\bin\nant.exe" ^
	"/f:%PathToThisBatFileFolder%NantScript.xml" ^
	%NAntTargetsToRun%

if not errorlevel 1 (
	powershell write-host -foregroundcolor green "SUCCESS!!!"
) else (
	powershell write-host -foregroundcolor red "FAILURE!!!"
)

pause