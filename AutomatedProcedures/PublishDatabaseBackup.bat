@echo off

echo PublishDatabaseBackup.bat:
echo Target audience: Deployment engineer;
echo Purpose: Create DB.bak based on current innovator state and publish it

SET NAntTargetsToRun=Publish.DB.Backup
SET PathToThisBatFileFolder=%~dp0
SET NAntParameters=%*

"%PathToThisBatFileFolder%tools\nant-0.92\bin\nant.exe" "/f:%PathToThisBatFileFolder%NantScript.xml" %NAntTargetsToRun%

if not errorlevel 1 (
	powershell write-host -foregroundcolor green "SUCCESS!!!"
) else (
	powershell write-host -foregroundcolor red "FAILURE!!!"
)

pause