DECLARE @MyFileName varchar(1000)
SELECT @MyFileName = (SELECT 'C:\SolidCad\Development\_CleanDatabaseBackups\InnovatorWEFIC12SP9_' + REPLACE(convert(nvarchar(20),GetDate(),120),':','-') + '.bak')
BACKUP DATABASE [InnovatorWEFIC12SP9] TO DISK=@MyFileName