@echo off

echo SetupInnovatorFromBaseline.bat:
echo Target audience: Developer team, QA team
echo Purpose: Setup local instance of Innovator from production backups

SET PathToThisBatFileFolder=%~dp0

FOR /f "delims=" %%i in ('git rev-parse --abbrev-ref HEAD') DO SET BranchName=%%i
FOR /f "delims=" %%i in ('git rev-parse --short HEAD') DO SET ShortCommitSha1=%%i

SET BuildSpecificName=%BranchName%-%ShortCommitSha1%

SET NAntParameters=-D:Name.Of.Innovator.Instance=%BuildSpecificName%

SET /P IISVirtualDirForTemporaryInstance=Enter IIS.VirtualDir.For.Temporary.Instance (Press enter to use default %BuildSpecificName% value): 
IF NOT "%IISVirtualDirForTemporaryInstance%"=="" (
	SET NAntParameters=%NAntParameters% -D:IIS.VirtualDir.For.Temporary.Instance="%IISVirtualDirForTemporaryInstance%"
)
REM SET PathToSetupRootDirectory="R:\Setup\Innovator\To\This\Folder\%BuildSpecificName%"
IF NOT "%PathToSetupRootDirectory%"=="" (
	SET NAntParameters=%NAntParameters% -D:Path.To.Installed.Innovator="%PathToSetupRootDirectory%"
)

SET NAntParameters=%NAntParameters% -D:MSSQL.Database.Name=%BuildSpecificName% 
SET NAntParameters=%NAntParameters% -D:Agent.Service.Name=ArasInnovatorAgent_AgentService_%BuildSpecificName%

"%PathToThisBatFileFolder%tools\nant-0.92\bin\nant.exe" ^
	"/f:%PathToThisBatFileFolder%NantScript.xml" ^
	Setup.Innovator.From.Backups ^
	%NAntParameters%

if not errorlevel 1 (
	powershell write-host -foregroundcolor green "SUCCESS!!!"
) else (
	powershell write-host -foregroundcolor red "FAILURE!!!"
)

pause