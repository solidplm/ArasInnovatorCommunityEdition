@echo off

echo Deploy.bat:
echo Target audience: Deployment engineer, QA team;
echo Purpose: Deploy upgrade to the target server

IF "%NAntTargetsToRun%"=="" ( SET NAntTargetsToRun=Deploy )
SET PathToThisBatFileFolder=%~dp0
SET NAntParameters=%*

Call:InputPassword "Enter password of root Innovator user" InnovatorRootPassword

"%PathToThisBatFileFolder%tools\nant-0.92\bin\nant.exe" "/f:%PathToThisBatFileFolder%NantScript.xml" %NAntTargetsToRun% -D:Password.Of.Root.Innovator.User="%InnovatorRootPassword%" %NAntParameters%

if not errorlevel 1 (
	powershell write-host -foregroundcolor green "SUCCESS!!!"
) else (
	powershell write-host -foregroundcolor red "FAILURE!!!"
)

pause
goto :eof

:InputPassword
echo.
set "psCommand=powershell -command "$pword = read-host '%1' -AsSecureString ; ^
    $BSTR=[System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($pword); ^
      [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)""
        for /f "usebackq delims=" %%p in (`%psCommand%`) do set %2=%%p
)
goto :eof