@echo off

echo RestoreInnovatorFromBackups.bat
echo Target audience: QA team
echo Purpose: Restore instance of Innovator from code tree and database backups

SET NAntTargetsToRun=RestoreInnovator
SET PathToThisBatFileFolder=%~dp0

rem If you want to pass some parameters to NAnt script from here then you can pass them via -D:<name>=<value>.
rem For example:
rem -D:MSSQL.Server=(local)\sql2012
"%PathToThisBatFileFolder%tools\nant-0.92\bin\nant.exe" "/f:%PathToThisBatFileFolder%NantScript.xml" %NAntTargetsToRun%

if not errorlevel 1 (
	powershell write-host -foregroundcolor green "SUCCESS!!!"
) else (
	powershell write-host -foregroundcolor red "FAILURE!!!"
)

pause