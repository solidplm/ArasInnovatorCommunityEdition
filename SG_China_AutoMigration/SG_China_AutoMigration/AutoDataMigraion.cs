﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aras.IOM;
using System.IO;
using System.Web;

namespace SG_China_AutoMigration
{
    public class AutoDataMigraion : Item
    {

        public AutoDataMigraion(IServerConnection arg) : base(arg)
        {

        }
        public Item SG_China_AutoDataMigration(Item item)
        {

            //System.Diagnostics.Debugger.Break();
            Innovator SourceInnovator = item.getInnovator();
            Innovator inn = item.getInnovator();

            string itemId = item.getID();
            string tbl_name = item.getType();
            string Itemnumber = item.getProperty("item_number", "");
            string When = "Auto Data Migration";
            string ExceptionItem_id = "";
            string sg_current_state = "";
            try
            {
                /** check migration **/
                Item Exception_Add = SourceInnovator.newItem("migration_items", "add");
                Exception_Add.setProperty("m_type", tbl_name);
                Exception_Add.setProperty("m_id", itemId);
                Exception_Add.setProperty("m_item_number", Itemnumber);
                Exception_Add.setProperty("m_when", When);
                Exception_Add.setProperty("m_reason", "");
                Exception_Add.setProperty("m_check_migration", "InProgress");
                Exception_Add = Exception_Add.apply();
                ExceptionItem_id = Exception_Add.getID();
                Innovator DestinationInnovator = GetDestinationInnovator(SourceInnovator);

                if (tbl_name == "Part")
                {

                    Item SourcePart = SourceInnovator.newItem("part", "get");
                    SourcePart.setID(itemId);
                    SourcePart = SourcePart.apply();
                    string itemNumber = SourcePart.getProperty("item_number");
                    string config_id = SourcePart.getProperty("config_id");
                    string itemID = SourcePart.getID();
                    string china_current_state = SourcePart.getProperty("current_state", "");
                    string china_state = SourcePart.getProperty("state", "");
                    string release_date = SourcePart.getProperty("release_date", "");
                    string modified_on = SourcePart.getProperty("modified_on", "");
                    string reviewed_by = SourcePart.getProperty("reviewed_by", "");
                    string released_by = SourcePart.getProperty("released_by", "");
                    string effective_date = SourcePart.getProperty("effective_date", "");
                    string is_released = SourcePart.getProperty("is_released", "");

                    Item DestinationPart = DestinationInnovator.newItem("Part", "get");
                    DestinationPart.setProperty("config_id", config_id);
                    DestinationPart.setProperty("is_current", "1");
                    DestinationPart = DestinationPart.apply();
                    int ChinaPartCount = 0;

                    ChinaPartCount = DestinationPart.getItemCount();
                    // string old_part_id = "";

                    if (ChinaPartCount < 1)
                    {

                        /******* Part Insert *********/
                        Item insertedPart = InsertPartUsingSQL(SourceInnovator, DestinationInnovator, SourcePart);

                    }
                    if (ChinaPartCount > 0)
                    {

                        string ExistingPartID = DestinationPart.getID();

                        /** If Part is already there **/
                        /** Get Part with is_current = 1 and Id **/
                        StringBuilder sb_same_id_item = new StringBuilder("");
                        sb_same_id_item.Append(" SELECT * ");
                        sb_same_id_item.Append(" FROM innovator.[part]");
                        sb_same_id_item.Append(" WHERE config_id='" + config_id + "'");
                        sb_same_id_item.Append(" AND is_current=1");
                        sb_same_id_item.Append(" AND id='" + itemID + "'");
                        string sql_same_id_item = sb_same_id_item.ToString();
                        Item same_id_item = DestinationInnovator.applySQL(sql_same_id_item);

                        //Item same_id_item = DestinationInnovator.applySQL(
                        //    "SELECT * FROM  innovator.[part]" + " WHERE item_number='" + itemNumber + "'" + " AND is_current=1 AND id='" + itemID + "'");

                        /** If China have same ID Part - Check State **/
                        if (same_id_item.getItemCount() > 0)
                        {
                            sg_current_state = same_id_item.getProperty("current_state", "");

                            if (sg_current_state != china_current_state)
                            {
                                StringBuilder sb_update_state = new StringBuilder("");
                                sb_update_state.Append(" UPDATE innovator.[Part]");
                                sb_update_state.Append(" SET  current_state ='" + china_current_state + "'");
                                sb_update_state.Append(" ,  state ='" + china_state + "'");
                                sb_update_state.Append(" ,  release_date ='" + release_date + "'");
                                sb_update_state.Append(" ,  modified_on ='" + modified_on + "'");
                                if (reviewed_by != "")
                                {
                                    sb_update_state.Append(" ,  reviewed_by ='" + reviewed_by + "'");
                                }
                                if (released_by != "")
                                {
                                    sb_update_state.Append(" ,  released_by ='" + released_by + "'");
                                }
                                if (effective_date != "")
                                {
                                    sb_update_state.Append(" ,  effective_date ='" + effective_date + "'");
                                }
                                sb_update_state.Append(" ,  is_released ='" + is_released + "'");
                                sb_update_state.Append(" WHERE config_id='" + config_id + "'");
                                sb_update_state.Append(" AND is_current=1 ");
                                string sql_update_state = sb_update_state.ToString();
                                Item update_state = DestinationInnovator.applySQL(sql_update_state);
                                if (update_state.isError())
                                {
                                    return DestinationInnovator.newError("Error : Update State : " + update_state.getErrorDetail());
                                }
                            }
                        }

                        /** Not Same ID - Revised Version - (1) Update old revision (2) Insert new revision **/
                        if (same_id_item.getItemCount() < 1)
                        {

                            /** (1) Update old revision **/
                            StringBuilder sb_update_uniqueness = new StringBuilder("");
                            sb_update_uniqueness.Append(" UPDATE innovator.[part]");
                            sb_update_uniqueness.Append(" SET  [ARAS:UNIQUENESS_HELPER]='" + ExistingPartID + "',");
                            sb_update_uniqueness.Append(" is_current = 0");
                            sb_update_uniqueness.Append(" WHERE config_id='" + config_id + "'");
                            sb_update_uniqueness.Append(" AND is_current=1");
                            string sql_update_uniqueness = sb_update_uniqueness.ToString();
                            Item update_uniqueness = DestinationInnovator.applySQL(sql_update_uniqueness);

                            if (update_uniqueness.isError())
                            {
                                return SourceInnovator.newError("Error at UPDATE UNIQUENESS");
                            }

                            /** (2) Insert new revision **/
                            Item insertedPart = InsertPartUsingSQL(SourceInnovator, DestinationInnovator, SourcePart);
                            if (insertedPart.isError())
                            {
                                return SourceInnovator.newError("Error at InsertPartUsingSQL after update." + ":" + insertedPart.getErrorDetail());
                            }
                        }

                        string new_part_id = "";

                        StringBuilder sb_new_part_item = new StringBuilder("");
                        sb_new_part_item.Append(" SELECT * ");
                        sb_new_part_item.Append(" FROM  innovator.[part]");
                        sb_new_part_item.Append(" WHERE item_number='" + itemNumber + "'");
                        sb_new_part_item.Append(" AND is_current=1");
                        string sql_new_part_item = sb_new_part_item.ToString();
                        Item new_part_item = DestinationInnovator.applySQL(sql_new_part_item);

                        //Item new_part_item = DestinationInnovator.applySQL(
                        //    "SELECT * FROM  innovator.[part]" + " WHERE item_number='" + itemNumber + "'" + " AND is_current=1");
                        if (!new_part_item.isError() && new_part_item.getItemCount() == 1)
                        {
                            new_part_id = new_part_item.getID();
                        }

                        /** Update Part BOM **/
                        StringBuilder sb_bom_update = new StringBuilder("");
                        sb_bom_update.Append(" UPDATE innovator.PART_BOM");
                        sb_bom_update.Append(" SET related_id = '" + new_part_id + "'");
                        sb_bom_update.Append(" WHERE ID IN (");
                        sb_bom_update.Append("	SELECT ID");
                        sb_bom_update.Append("	FROM innovator.PART_BOM");
                        sb_bom_update.Append("	WHERE related_id IN (");
                        sb_bom_update.Append("		SELECT ID");
                        sb_bom_update.Append("		FROM innovator.PART");
                        sb_bom_update.Append("		WHERE config_id = '" + config_id + "'))");
                        string sql_bom_update = sb_bom_update.ToString();
                        Item bom_update_result = DestinationInnovator.applySQL(sql_bom_update);
                        if (bom_update_result.isError())
                        {
                            throw new Exception("Error : BOM UPDATE" + ":" + bom_update_result.getErrorDetail());
                        }
                    }


                    /*** Part BOM ***/
                    Item assemblyItem = SourceInnovator.newItem("Part", "get");
                    assemblyItem.setAttribute("select", "keyed_name");
                    assemblyItem.setID(itemID);
                    Item bomItem = SourceInnovator.newItem("Part BOM", "get");
                    bomItem.setAttribute("select", "sort_order,quantity,related_id(keyed_name)");
                    bomItem.setAttribute("orderby", "sort_order");
                    assemblyItem.addRelationship(bomItem);
                    assemblyItem = assemblyItem.apply();
                    if (!assemblyItem.isError())
                    {
                        Item bomItems = assemblyItem.getRelationships();
                        int bomItemsCount = bomItems.getItemCount();
                        if (bomItemsCount > 0)
                        {

                            AddPartBOM(SourceInnovator, DestinationInnovator, assemblyItem);

                            for (int j = 0; j < bomItemsCount; j++)
                            {
                                Item PartBOMItem = bomItems.getItemByIndex(j);
                                AddItemUsingSQL(SourceInnovator, DestinationInnovator, PartBOMItem);
                            }
                        }
                    }
                    /*** Part BOM ***/

                    /*** Part document ***/
                    Item assembly_Item_ = SourceInnovator.newItem("Part", "get");
                    assembly_Item_.setAttribute("select", "keyed_name");
                    assembly_Item_.setID(itemID);
                    Item documentItem = SourceInnovator.newItem("Part Document", "get");
                    documentItem.setAttribute("select", "sort_order,quantity,related_id(keyed_name)");
                    documentItem.setAttribute("orderby", "sort_order");
                    assembly_Item_.addRelationship(documentItem);
                    assembly_Item_ = assembly_Item_.apply();
                    if (!assembly_Item_.isError())
                    {
                        Item documentItems = assembly_Item_.getRelationships();
                        int DItemsCount = documentItems.getItemCount();
                        if (DItemsCount > 0)
                        {

                            AddDocument(SourceInnovator, DestinationInnovator, SourcePart);

                            for (int j = 0; j < DItemsCount; j++)
                            {
                                Item PartDocumentItem = documentItems.getItemByIndex(j);
                                AddItemUsingSQL(SourceInnovator, DestinationInnovator, PartDocumentItem);
                            }
                        }
                    }
                    /*** Part document ***/
                    /*** Part vendor ***/
                    Item Ass_Item = SourceInnovator.newItem("Part", "get");
                    Ass_Item.setAttribute("select", "keyed_name");
                    Ass_Item.setID(itemID);
                    Item vendorItem = SourceInnovator.newItem("Part Vendor", "get");
                    vendorItem.setAttribute("select", "sort_order,quantity,related_id(keyed_name)");
                    vendorItem.setAttribute("orderby", "sort_order");
                    Ass_Item.addRelationship(vendorItem);
                    Ass_Item = Ass_Item.apply();
                    if (!Ass_Item.isError())
                    {
                        Item vendorItems = Ass_Item.getRelationships();
                        int VItemsCount = vendorItems.getItemCount();
                        if (VItemsCount > 0)
                        {

                            AddVendor(SourceInnovator, DestinationInnovator, SourcePart);

                            for (int j = 0; j < VItemsCount; j++)
                            {
                                Item PartVendorItem = vendorItems.getItemByIndex(j);
                                AddItemUsingSQL(SourceInnovator, DestinationInnovator, PartVendorItem);
                            }
                        }
                    }
                    /*** Part vendor ***/

                }

                if (tbl_name == "Document")
                {

                    Item SGDocument = SourceInnovator.newItem("Document", "get");
                    SGDocument.setID(itemId);
                    SGDocument = SGDocument.apply();
                    string itemNumber = SGDocument.getProperty("item_number");
                    string config_id = SGDocument.getProperty("config_id");
                    string itemID = SGDocument.getID();

                    string china_current_state = SGDocument.getProperty("current_state", "");
                    string china_state = SGDocument.getProperty("state", "");
                    string release_date = SGDocument.getProperty("release_date", "");
                    string modified_on = SGDocument.getProperty("modified_on", "");
                    string reviewed_by = SGDocument.getProperty("reviewed_by", "");
                    string released_by = SGDocument.getProperty("released_by", "");
                    string effective_date = SGDocument.getProperty("effective_date", "");
                    string is_released = SGDocument.getProperty("is_released", "");

                    Item Document_China = DestinationInnovator.newItem("Document", "get");
                    Document_China.setProperty("config_id", config_id);
                    Document_China.setProperty("is_current", "1");
                    Document_China = Document_China.apply();
                    int Document_China_Count = 0;
                    string _id_ = "";
                    
                    Document_China_Count = Document_China.getItemCount();
                    /** When Document is not already existed in Destination, Insert that document **/
                    if (Document_China_Count < 1)
                    {
                        InsertDocumentUsingSQL(SourceInnovator, DestinationInnovator, SGDocument);
                    }
                    else
                    {
                        string new_documentID = "";
                        string partDocument_ID = "";
                        /** If Document is already existed in Destination **/
                        StringBuilder sb_check_D_iscurrentOne = new StringBuilder("");
                        sb_check_D_iscurrentOne.Append("	SELECT *");
                        sb_check_D_iscurrentOne.Append("	FROM innovator.document");
                        sb_check_D_iscurrentOne.Append(" WHERE config_id = '" + config_id + "'" + " AND is_current=1");
                        string sql_sb_check_D_iscurrentOne = sb_check_D_iscurrentOne.ToString();
                        Item result_Document = DestinationInnovator.applySQL(sql_sb_check_D_iscurrentOne);

                        if (result_Document.getItemCount() > 0)
                        {
                            StringBuilder sb_check_D_withID = new StringBuilder("");
                            sb_check_D_withID.Append("	SELECT * ");
                            sb_check_D_withID.Append("	FROM innovator.document");
                            sb_check_D_withID.Append(" WHERE config_id = '" + config_id + "'" + " AND is_current=1 and id='" + itemID + "'");
                            string sql_sb_check_D_withID = sb_check_D_withID.ToString();
                            Item result_Document_sameID = DestinationInnovator.applySQL(sql_sb_check_D_withID);

                            if (result_Document_sameID.getItemCount() > 0)
                            {
                                sg_current_state = result_Document_sameID.getProperty("current_state", "");

                                if (sg_current_state != china_current_state)
                                {
                                    StringBuilder sb_update_state = new StringBuilder("");
                                    sb_update_state.Append(" UPDATE innovator.[Document]");
                                    sb_update_state.Append(" SET  current_state ='" + china_current_state + "'");
                                    sb_update_state.Append(" ,  state ='" + china_state + "'");
                                    sb_update_state.Append(" ,  release_date ='" + release_date + "'");
                                    sb_update_state.Append(" ,  modified_on ='" + modified_on + "'");
                                    if (reviewed_by != "")
                                    {
                                        sb_update_state.Append(" ,  reviewed_by ='" + reviewed_by + "'");
                                    }
                                    if (released_by != "")
                                    {
                                        sb_update_state.Append(" ,  released_by ='" + released_by + "'");
                                    }
                                    if (effective_date != "")
                                    {
                                        sb_update_state.Append(" ,  effective_date ='" + effective_date + "'");
                                    }
                                    sb_update_state.Append(" ,  is_released ='" + is_released + "'");
                                    sb_update_state.Append(" WHERE config_id='" + config_id + "'");
                                    sb_update_state.Append(" AND is_current=1 ");
                                    string sql_update_state = sb_update_state.ToString();
                                    Item update_state = DestinationInnovator.applySQL(sql_update_state);
                                    if (update_state.isError())
                                    {
                                        return DestinationInnovator.newError("Error : Update State : " + update_state.getErrorDetail());
                                    }
                                }
                            }
                            /** If Document is already existed in Destination with not the same ID **/
                            if (result_Document_sameID.getItemCount() < 1)
                            {
                                StringBuilder sb_document_getoldID = new StringBuilder("");

                                sb_document_getoldID.Append("	SELECT *");
                                sb_document_getoldID.Append("	FROM innovator.document");
                                sb_document_getoldID.Append(" WHERE config_id = '" + config_id + "'" + " AND is_current=1");
                                string sql_sb_document_getoldID = sb_document_getoldID.ToString();
                                Item _result_ = DestinationInnovator.applySQL(sql_sb_document_getoldID);

                                var _count_ = _result_.getItemCount();
                                if (_count_ > 0)
                                {
                                    var result_item = _result_.getItemByIndex(0);
                                    _id_ = result_item.getProperty("id");

                                    StringBuilder sb_document_update = new StringBuilder("");
                                    sb_document_update.Append(" UPDATE innovator.Document");
                                    sb_document_update.Append(" SET [ARAS:UNIQUENESS_HELPER] = '" + _id_ + "'" + " ,is_current = 0");
                                    sb_document_update.Append(" WHERE config_id = '" + config_id + "'" + " AND is_current=1");
                                    string sql_sb_document_update = sb_document_update.ToString();
                                    Item document_update_result = DestinationInnovator.applySQL(sql_sb_document_update);
                                    if (document_update_result.isError())
                                    {
                                        return SourceInnovator.newError("Error at UPDATE UNIQUENESS");
                                        throw new Exception("Error : UPDATE [ARAS:UNIQUENESS_HELPER]");
                                    }

                                    /** insert document **/
                                    InsertDocumentUsingSQL(SourceInnovator, DestinationInnovator, SGDocument);
                                }
                            }
                        }                       

                        /** update places where used with old_Id to be latest **/
                        StringBuilder sb_partDocument = new StringBuilder("");
                        sb_partDocument.Append("	SELECT * ");
                        sb_partDocument.Append("	FROM innovator.[part_document]");
                        sb_partDocument.Append(" WHERE related_id = '" + _id_ + "'");
                        string sql_sb_partDocument = sb_partDocument.ToString();
                        Item partDocument = DestinationInnovator.applySQL(sql_sb_partDocument);

                        var partDocument_count = partDocument.getItemCount();
                        for (int j = 0; j < partDocument_count; j++)
                        {
                            var partDocument_Item = partDocument.getItemByIndex(j);
                            partDocument_ID = partDocument_Item.getProperty("id");
                            
                            StringBuilder sb_newDocument = new StringBuilder("");
                            sb_newDocument.Append("	SELECT *");
                            sb_newDocument.Append("	FROM innovator.document");
                            sb_newDocument.Append(" WHERE item_number = '" + itemNumber + "'" + " AND is_current=1");
                            string sql_sb_newDocument = sb_newDocument.ToString();
                            Item newDocument = DestinationInnovator.applySQL(sql_sb_newDocument);
                            
                            new_documentID = newDocument.getProperty("id");

                            StringBuilder sb_oldDocument_allupdate = new StringBuilder("");
                            sb_oldDocument_allupdate.Append(" UPDATE innovator.part_document");
                            sb_oldDocument_allupdate.Append(" SET related_id = '" + new_documentID + "'");
                            sb_oldDocument_allupdate.Append(" WHERE id = '" + partDocument_ID + "'");
                            string sql_sb_oldDocument_allupdate = sb_oldDocument_allupdate.ToString();
                            Item oldDocument_allupdate_result = DestinationInnovator.applySQL(sql_sb_oldDocument_allupdate);
                        }
                    }

                    /*** document file ***/
                    Item documentItem = SourceInnovator.newItem("Document", "get");
                    documentItem.setAttribute("select", "id");
                    documentItem.setID(itemID);

                    Item documentFileItem = SourceInnovator.newItem("Document File", "get");
                    documentFileItem.setAttribute("select", "related_id(id,filename)");
                    documentItem.addRelationship(documentFileItem);

                    Item resultDocuments = documentItem.apply();

                    if (resultDocuments.isError())
                    {
                        return SourceInnovator.newError("Item not found: " + resultDocuments.getErrorDetail());
                    }

                    Item df_Items = resultDocuments.getRelationships();
                    int df_count = df_Items.getItemCount();

                    for (int j = 0; j < df_count; ++j)
                    {
                        Item df_item = df_Items.getItemByIndex(j);
                        Item related_fileItem = df_item.getRelatedItem();

                        string fileID = related_fileItem.getID();
                        string file_Name = related_fileItem.getProperty("filename", "");

                        Item _getFiles = DestinationInnovator.applySQL("select * From innovator.[FILE]  where id ='" + fileID + "'");
                        Item getFiles = SourceInnovator.applySQL("select * From innovator.[FILE]  where id ='" + fileID + "'");
                        string fileName = "";
                        Item addFile = DestinationInnovator.newItem();
                        if (getFiles.getItemCount() > 0)
                        {
                            fileName = getFiles.getProperty("filename");
                        }

                        string filePath = FileDownloadAndUpload(SourceInnovator, fileID);

                        if (!string.IsNullOrEmpty(filePath))
                        {
                            if (_getFiles.getItemCount() < 1)
                            {
                                addFile = DestinationInnovator.newItem("File", "add");

                                addFile.setID(fileID);
                                addFile.setProperty("filename", fileName);
                                addFile.attachPhysicalFile(filePath);
                                addFile = addFile.apply();
                                if (addFile.isError())
                                {
                                    return SourceInnovator.newError("Error at attach physical file into China.");
                                }
                            }
                        }
                        string item_ID = df_item.getProperty("id", "");

                        StringBuilder sb_DFile = new StringBuilder("");

                        sb_DFile.Append("	SELECT * ");
                        sb_DFile.Append("	FROM innovator.[document_file]");
                        sb_DFile.Append(" WHERE id = '" + item_ID + "'" + " AND is_current=1");
                        string sql_sb_DFile = sb_DFile.ToString();
                        Item DFile = DestinationInnovator.applySQL(sql_sb_DFile);
                        if (DFile.getItemCount() < 1)
                        {
                            InsertItemUsingSQL(SourceInnovator, DestinationInnovator, df_item);
                        }
                    }
                    /*** document file ***/
                }
                // success
                const string SUCCESS = "Success";
                string migrationItemId = "";
                Item success_update = SourceInnovator.newItem("migration_items", "get");
                //   success_update.setProperty("m_item_number", Itemnumber);
                //   success_update.setProperty("m_id", itemId);
                success_update.setProperty("id", ExceptionItem_id);
                success_update = success_update.apply();
                if (success_update.getItemCount() > 0)
                {
                    migrationItemId = success_update.getID();
                    StringBuilder sb_update_check = new StringBuilder("");
                    sb_update_check.Append(" UPDATE innovator.migration_items");
                    sb_update_check.Append(" SET m_check_migration = 'Success' ");
                    sb_update_check.Append(" WHERE id = '" + migrationItemId + "'");
                    string sql_sb_update_check = sb_update_check.ToString();
                    Item update_check_result = SourceInnovator.applySQL(sql_sb_update_check);
                }
                Item _migration_Item = SourceInnovator.getItemById("migration_items", migrationItemId);
                Item _result_Item = _migration_Item.promote(SUCCESS, "");
            }
            catch (Exception ee)
            {
                var error = SourceInnovator.newError(ee.Message);
                string error_reason = error.getErrorString();
                error_reason = error_reason.Replace("'", "''");
                //  Item Exception_Add = SourceInnovator.newItem("migration_items", "add");
                //  Exception_Add.setProperty("m_type", tbl_name);
                //  Exception_Add.setProperty("m_id", itemId);
                //  Exception_Add.set.Property("m_item_number", Itemnumber);
                //  Exception_Add.setProperty("m_when", When);
                //  Exception_Add.setProperty("m_reason", error_reason);
                //  Exception_Add = Exception_Add.apply();
                const string FAILED = "Failed";

                string migrationItem_id = "";
                Item migrationItem = SourceInnovator.newItem();
                if (error_reason == "The system cannot get connection to China Aras to release this item.")
                {
                    Item Exception_update = SourceInnovator.newItem("migration_items", "get");
                    //   Exception_update.setProperty("m_item_number", Itemnumber);
                    //   Exception_update.setProperty("m_id", itemId);
                    Exception_update.setProperty("id", ExceptionItem_id);
                    Exception_update = Exception_update.apply();
                    if (Exception_update.getItemCount() > 0)
                    {
                        migrationItem_id = Exception_update.getID();
                        StringBuilder sb_update_check = new StringBuilder("");
                        sb_update_check.Append(" UPDATE innovator.migration_items");
                        sb_update_check.Append(" SET m_reason = '" + error_reason + "'" + ",m_check_migration = 'Failed' ");
                        sb_update_check.Append(" WHERE id = '" + migrationItem_id + "'");
                        string sql_sb_update_check = sb_update_check.ToString();
                        Item update_check_result = SourceInnovator.applySQL(sql_sb_update_check);
                    }
                    migrationItem = SourceInnovator.getItemById("migration_items", migrationItem_id);
                    Item result_Item = migrationItem.promote(FAILED, "");
                }
                else
                {
                    Item Exception_update = SourceInnovator.newItem("migration_items", "get");
                    //   Exception_update.setProperty("m_item_number", Itemnumber);
                    //   Exception_update.setProperty("m_id", itemId);
                    Exception_update.setProperty("id", ExceptionItem_id);
                    Exception_update = Exception_update.apply();
                    if (Exception_update.getItemCount() > 0)
                    {
                        migrationItem_id = Exception_update.getID();
                        StringBuilder sb_update_check = new StringBuilder("");
                        sb_update_check.Append(" UPDATE innovator.migration_items");
                        sb_update_check.Append(" SET m_reason = '" + error_reason + "'");
                        sb_update_check.Append(" WHERE id = '" + migrationItem_id + "'");
                        string sql_sb_update_check = sb_update_check.ToString();
                        Item update_check_result = SourceInnovator.applySQL(sql_sb_update_check);
                    }
                    migrationItem = SourceInnovator.getItemById("migration_items", migrationItem_id);
                    Item result_Item = migrationItem.promote(FAILED, "");
                }
            }

            return this;
            // return SourceInnovator.newError("Return Error Intentionally.");
        }

        public static void AddPartBOM(Innovator SourceInnovator, Innovator DestinationInnovator, Item partItem)
        {
            // something recurrsive
            // Add Related Part 
            string itemID = partItem.getID();
            string itemTypeName = partItem.getType();
            string itemKeyedName = partItem.getProperty("keyed_name", "");

            StringBuilder sb_get_permission = new StringBuilder("");
            sb_get_permission.Append(" select * ");
            sb_get_permission.Append(" from innovator.part ");
            sb_get_permission.Append(" where id = '" + itemID + "'");
            string sql_get_permission = sb_get_permission.ToString();
            Item get_permission = SourceInnovator.applySQL(sql_get_permission);

            //    Item get_permission = SourceInnovator.applySQL("select * " +
            //"from innovator.part " +
            //"where id = '" + itemID + "'");
            string permission_id = get_permission.getProperty("permission_id", "");
            string config_id = get_permission.getProperty("config_id", "");

            Item destinationItem = DestinationInnovator.newItem(itemTypeName, "get");
            // destinationItem.setProperty("keyed_name", itemKeyedName);
            //destinationItem.setProperty("is_current", "1");
            destinationItem.setProperty("id", itemID);
            destinationItem = destinationItem.apply();

            if (!destinationItem.isError() && destinationItem.getItemCount() > 0)
            {
                /** DO NOTHING**/
                //return same_id_item;
                // continue;
            }
            else
            {
                StringBuilder sb_partItemDestination = new StringBuilder("");

                sb_partItemDestination.Append("	SELECT *");
                sb_partItemDestination.Append("	FROM innovator.part");
                sb_partItemDestination.Append(" WHERE config_id = '" + config_id + "'" + " AND is_current=1");
                string sql_sb_partItemDestination = sb_partItemDestination.ToString();
                Item partItemDestination = DestinationInnovator.applySQL(sql_sb_partItemDestination);

                /** When part is not already existed in China, Insert that part **/
                if (partItemDestination.getItemCount() < 1)
                {
                    AddItemUsingSQL(SourceInnovator, DestinationInnovator, partItem);
                }

                /** If part is already existed in China **/

                /** (1) Same ID  - DO NOTHING **/
                /** (2) Different ID - UPDATE AND INSERT  **/
                if (partItemDestination.getItemCount() > 0)
                {
                    string destination_id = partItemDestination.getItemByIndex(0).getID();

                    if (itemID != destination_id)
                    {
                        UpdateItemUsingSQL(SourceInnovator, DestinationInnovator, partItem);
                    }
                }
            }
            /*** add part Document ***/
            Item assembly_Item = SourceInnovator.newItem("Part", "get");
            assembly_Item.setAttribute("select", "keyed_name");
            assembly_Item.setID(itemID);
            Item documentItem = SourceInnovator.newItem("Part Document", "get");
            documentItem.setAttribute("select", "sort_order,quantity,related_id(keyed_name)");
            documentItem.setAttribute("orderby", "sort_order");
            assembly_Item.addRelationship(documentItem);
            assembly_Item = assembly_Item.apply();
            if (!assembly_Item.isError())
            {
                Item documentItems = assembly_Item.getRelationships();
                int DItemsCount = documentItems.getItemCount();
                if (DItemsCount > 0)
                {
                    AddDocument(SourceInnovator, DestinationInnovator, partItem);

                    for (int j = 0; j < DItemsCount; j++)
                    {
                        Item PartDocumentItem = documentItems.getItemByIndex(j);
                        AddItemUsingSQL(SourceInnovator, DestinationInnovator, PartDocumentItem);
                    }
                }
            }

            /*** add part Document ***/
            /*** add part vendor ***/
            Item assembly_VItem = SourceInnovator.newItem("Part", "get");
            assembly_VItem.setAttribute("select", "keyed_name");
            assembly_VItem.setID(itemID);
            Item vendorItem = SourceInnovator.newItem("Part Vendor", "get");
            vendorItem.setAttribute("select", "sort_order,quantity,related_id(keyed_name)");
            vendorItem.setAttribute("orderby", "sort_order");
            assembly_VItem.addRelationship(vendorItem);
            assembly_VItem = assembly_VItem.apply();
            if (!assembly_VItem.isError())
            {
                Item vendorItems = assembly_VItem.getRelationships();
                int VItemsCount = vendorItems.getItemCount();
                if (VItemsCount > 0)
                {
                    AddVendor(SourceInnovator, DestinationInnovator, partItem);

                    for (int j = 0; j < VItemsCount; j++)
                    {
                        Item PartVedorItem = vendorItems.getItemByIndex(j);
                        AddItemUsingSQL(SourceInnovator, DestinationInnovator, PartVedorItem);
                    }
                }
            }

            /*** add part vendor ***/


            Item qryItem = SourceInnovator.newItem("Part", "get");
            qryItem.setAttribute("select", "keyed_name,item_number,description,cost");
            qryItem.setID(partItem.getID());

            // Add the BOM structure.
            Item bomItem = SourceInnovator.newItem("Part BOM", "get");
            bomItem.setAttribute("select", "sort_order,quantity,related_id(keyed_name,item_number,description,cost)");
            bomItem.setAttribute("orderby", "sort_order");
            qryItem.addRelationship(bomItem);

            // Perform the query.
            Item results = qryItem.apply();
            // Test for an error.
            if (!results.isError())
            {
                // Get a handle to the BOM Items.
                Item bomItems = results.getRelationships();
                int bomItemsCount = bomItems.getItemCount();
                for (int i = 0; i < bomItemsCount; ++i)
                {

                    Item bom = bomItems.getItemByIndex(i);
                    Item bomPart = bom.getRelatedItem();
                    AddPartBOM(SourceInnovator, DestinationInnovator, bomPart);
                    AddItemUsingSQL(SourceInnovator, DestinationInnovator, bom);
                }
            }
        }


        public static Item AddItemUsingSQL(Innovator SourceInnovator, Innovator DestinationInnovator, Item sourceItem)
        {
            string itemID = sourceItem.getProperty("id", "");
            string itemKeyedName = sourceItem.getProperty("keyed_name", "");
            string itemTypeName = sourceItem.getType();

            Item chinaItem = DestinationInnovator.newItem(itemTypeName, "get");
            // chinaItem.setID(itemID);
            chinaItem.setProperty("id", itemID);
            chinaItem.setPropertyCondition("id", "eq");
            chinaItem = chinaItem.apply();

            // If error. there is no item with that ID
            if (chinaItem.isError())
            {
                // throw (new Exception("Error: AddItemUsingSQL function for " + itemKeyedName + "Error Detail:\n" + chinaItem.getErrorDetail()));
                InsertItemUsingSQL(SourceInnovator, DestinationInnovator, sourceItem);
            }

            if (chinaItem.getItemCount() > 0)
            {
                UpdateItemUsingSQL(SourceInnovator, DestinationInnovator, sourceItem);
            }

            return SourceInnovator.newResult("Success");
        }

        public static Item InsertItemUsingSQL(Innovator SourceInnovator, Innovator DestinationInnovator, Item sourceItem)
        {
            /** Using SQL **/
            string itemID = sourceItem.getProperty("id", "");
            string itemKeyedName = sourceItem.getProperty("keyed_name", "");
            string itemTypeName = sourceItem.getType();
            //if (itemTypeName == "Part BOM")itemTypeName = "Part_BOM";

            if (itemTypeName == "Part BOM")
            {
                itemTypeName = "Part_BOM";
            }
            else if (itemTypeName == "Part Document")
            {
                itemTypeName = "Part_Document";
            }
            else if (itemTypeName == "Document File")
            {
                itemTypeName = "Document_File";
            }
            else if (itemTypeName == "Part Vendor")
            {
                itemTypeName = "Part_Vendor";
            }


            List<Item> propertyList = GetPropertyItems(SourceInnovator, itemTypeName);

            Item sqlItem = GetItemUsingSQL(SourceInnovator, sourceItem);

            StringBuilder sb_insert = new StringBuilder("");
            sb_insert.Append("INSERT INTO [innovator].[" + itemTypeName + "](");

            //loop for column names
            for (int i = 0; i < propertyList.Count; i++)
            {
                Item propertyItem = propertyList[i];
                string columnName = propertyItem.getProperty("keyed_name");
                string columnValue = sqlItem.getProperty(columnName, "");
                if (columnValue != "")
                {
                    sb_insert.Append(columnName + ",");
                }
            }

            string str_column_names = sb_insert.ToString();
            str_column_names = str_column_names.Substring(0, str_column_names.Length - 1);
            str_column_names += ") VALUES (";
            sb_insert = new StringBuilder();

            // loop for values
            for (int i = 0; i < propertyList.Count; i++)
            {
                Item propertyItem = propertyList[i];
                string columnName = propertyItem.getProperty("keyed_name", "");
                string dataType = propertyItem.getProperty("data_type", "");

                string columnValue = sqlItem.getProperty(columnName, "").Replace("'", "''");
                if (columnValue != "")
                {

                    switch (dataType)
                    {
                        case "date":
                        case "string":
                        case "item":
                        case "list":
                        case "filter list":
                        case "mv_list":
                        case "text":
                        case "formatted text":
                        case "color":
                        case "color list":
                        case "image":
                        case "md5":
                        case "federated":
                        case "ml_string":
                        case "sequence":
                            {
                                columnValue = "'" + sqlItem.getProperty(columnName, "").Replace("'", "''") + "'";
                            }
                            break;

                        case "decimal":
                        case "float":
                        case "integer":
                            break;

                        default:
                            break;
                    }

                    sb_insert.Append(columnValue + ",");
                }
            }

            string sql_insert = sb_insert.ToString();
            sql_insert = sql_insert.Substring(0, sql_insert.Length - 1);
            sql_insert += ")";

            sql_insert = str_column_names + sql_insert;
            Item chinaItem = DestinationInnovator.applySQL(sql_insert);

            if (chinaItem.isError())
            {
                throw (new Exception("Error: AddItemUsingSQL function for " + itemKeyedName + "Error Detail:\n" + chinaItem.getErrorDetail()));
            }



            return chinaItem;
        }

        public static Item GetItemUsingSQL(Innovator SourceInnovator, Item sourceItem)
        {
            string itemID = sourceItem.getProperty("id", "");
            string itemKeyedName = sourceItem.getProperty("keyed_name", "");
            string itemTypeName = sourceItem.getType();
            //if (itemTypeName == "Part BOM") itemTypeName = "Part_BOM";

            if (itemTypeName == "Part BOM")
            {
                itemTypeName = "Part_BOM";
            }
            else if (itemTypeName == "Part Document")
            {
                itemTypeName = "Part_Document";
            }
            else if (itemTypeName == "Document File")
            {
                itemTypeName = "Document_File";
            }
            else if (itemTypeName == "Part Vendor")
            {
                itemTypeName = "Part_Vendor";
            }

            List<Item> propertyList = GetPropertyItems(SourceInnovator, itemTypeName);

            StringBuilder sb_select = new StringBuilder("");
            sb_select.Append("SELECT ");
            for (int i = 0; i < propertyList.Count; i++)
            {
                Item propertyItem = propertyList[i];
                string dataType = propertyItem.getProperty("data_type", "");
                string columnName = propertyItem.getProperty("keyed_name", "");
                switch (dataType)
                {
                    case "date":
                        {
                            columnName = "CAST(" + columnName + " AS DATETIME2) AS " + columnName;
                        }
                        break;

                    default:
                        break;
                }

                if (columnName != "document_owner")
                {
                    if (i == propertyList.Count - 1)
                    {
                        sb_select.Append(columnName);
                    }
                    else
                    {
                        sb_select.Append(columnName + ",");
                    }
                }
            }

            sb_select.Append(" FROM [innovator].[" + itemTypeName + "]");
            sb_select.Append("WHERE ID = '" + itemID + "'");
            string sql_select = sb_select.ToString();
            Item sqlItem = SourceInnovator.applySQL(sql_select);
            if (sqlItem.isError())
            {
                throw new Exception("Error : SQL_SELECT");
            }

            return sqlItem;
        }

        private static string FileDownloadAndUpload(Innovator sourceInnovator, string fileID)
        {
            Innovator DestinationInnovator = GetDestinationInnovator(sourceInnovator);

            string workingDirectory = getLocalWorkFolder(sourceInnovator, "admin");
            /* Viewable File */
            Item viewableItem = sourceInnovator.newItem("File", "get");
            viewableItem.setProperty("id", fileID);
            if (fileID.Length > 0)
                viewableItem = viewableItem.apply();
            string viewablefileName = viewableItem.getProperty("filename");
            string viewableFilePath = CheckOutFile(sourceInnovator, fileID, workingDirectory, viewablefileName);

            /*End Viewable File*/
            return viewableFilePath;
        }


        private static string getLocalWorkFolder(Innovator sourceInnovator, string username)
        {
            string FolderPath =
                default(string);
            Item Item_results =
                default(Item);
            Item qryitem = sourceInnovator.newItem("user", "get");
            qryitem.setProperty("login_name", username);
            Item_results = qryitem.apply();
            try
            {
                FolderPath = (string)(Item_results.getItemsByXPath("//Result/Item").getProperty("working_directory"));
                if (FolderPath.Length < 3)
                {
                    FolderPath = FolderPath + "\\";
                }
            }
            catch (Exception)
            {
                FolderPath = "";
            }

            return FolderPath;
        }

        public static string CheckOutFile(Innovator sourceInnovator, string fileID, string checkOutDirector, string fileName)
        {
            checkOutDirector = HttpContext.Current.Server.MapPath("Data");
            string FileDirectory = Path.Combine(checkOutDirector, fileName);
            if (File.Exists(FileDirectory))
            {
                File.Delete(FileDirectory);
            }

            Item get_file_request = sourceInnovator.newItem();
            get_file_request.setType("File");
            get_file_request.setAction("get");
            get_file_request.setID(fileID);
            Item fileItem = get_file_request.apply();
            Item checkoutResult = fileItem.checkout(checkOutDirector);
            if (checkoutResult.isError())
                throw new Exception(string.Format("Failed to check-out file with ID={0}", fileID));
            return FileDirectory;
        }

        public static Innovator GetDestinationInnovator(Innovator SourceInnovator)
        {
            HttpServerConnection myConnection;
            Innovator myNewInnovator;
            string newServerURL = "";
            string newServerDBName = "";
            string newServerUserName = "";
            string newServerPassword = "";
            Item urlVar = SourceInnovator.newItem("VARIABLE", "get");
            urlVar.setProperty("name", "NewServerURL");
            urlVar = urlVar.apply();
            newServerURL = urlVar.getProperty("value");
            Item dbVar = SourceInnovator.newItem("VARIABLE", "get");
            dbVar.setProperty("name", "NewServerDBName");
            dbVar = dbVar.apply();
            newServerDBName = dbVar.getProperty("value");
            Item userVar = SourceInnovator.newItem("VARIABLE", "get");
            userVar.setProperty("name", "NewServerUserName");
            userVar = userVar.apply();
            newServerUserName = userVar.getProperty("value");

            Item passVar = SourceInnovator.newItem("VARIABLE", "get");
            passVar.setProperty("name", "NewServerPassword");
            passVar = passVar.apply();
            newServerPassword = passVar.getProperty("value");

            /*End Get Data From Variable*/
            string webURL = newServerURL;
            string dbName = newServerDBName;
            string userName = newServerUserName;
            string passWord = newServerPassword;
            myConnection = IomFactory.CreateHttpServerConnection(webURL, dbName, userName, passWord);
            Item login_result = myConnection.Login();
            if (login_result.isError())
            {
                throw (new Exception("The system cannot get connection to China Aras to release this item."));
            }
            myNewInnovator = IomFactory.CreateInnovator(myConnection);
            return myNewInnovator;
        }




        public static Item AddDocument(Innovator sourceInnovator, Innovator destinationInnovator, Item sourcePart)
        {
            // Innovator DestinationInnovator = GetDestinationInnovator(sourceInnovator);
            int D_Itemcount = 0;
            string partDocument_ID = "";
            Item ChinaDocument = destinationInnovator.newItem();
            string SGParentID = sourcePart.getItemByIndex(0).getID();
            string partID = sourcePart.getID();

            /** Get Document From Existing Server and Add to NewServer **/
            StringBuilder sb_documentItem = new StringBuilder("");
            sb_documentItem.Append("	SELECT * ");
            sb_documentItem.Append("	FROM innovator.document");
            sb_documentItem.Append(" WHERE ID IN (");
            sb_documentItem.Append("	SELECT related_id ");
            sb_documentItem.Append("	  FROM innovator.part_document");
            sb_documentItem.Append("		WHERE source_id = '" + partID + "')");
            string sql_sb_documentItem = sb_documentItem.ToString();
            Item documentItem = sourceInnovator.applySQL(sql_sb_documentItem);

            D_Itemcount = documentItem.getItemCount();
            for (int c = 0; c < D_Itemcount; c++)
            {
                Item document_item = documentItem.getItemByIndex(c);
                string docItemNumber = document_item.getProperty("item_number");
                string d_id = document_item.getID();
                string config_id = document_item.getProperty("config_id");

                StringBuilder sb_same_id_item = new StringBuilder("");

                sb_same_id_item.Append(" SELECT * ");
                sb_same_id_item.Append(" FROM innovator.document");
                sb_same_id_item.Append(" WHERE ID = '" + d_id + "'");
                string sql_sb_same_id_item = sb_same_id_item.ToString();
                Item same_id_item = destinationInnovator.applySQL(sql_sb_same_id_item);

                if (!same_id_item.isError() && same_id_item.getItemCount() > 0)
                {
                    /** DO NOTHING**/
                    //return same_id_item;
                    continue;
                }

                StringBuilder sb_documentItemDestination = new StringBuilder("");
                sb_documentItemDestination.Append("	SELECT *");
                sb_documentItemDestination.Append("	FROM innovator.document");
                sb_documentItemDestination.Append(" WHERE config_id = '" + config_id + "'" + " AND is_current=1");
                string sql_sb_documentItemDestination = sb_documentItemDestination.ToString();
                Item documentItemDestination = destinationInnovator.applySQL(sql_sb_documentItemDestination);

                /** When Document is not already existed in Destination, Insert that document **/
                if (documentItemDestination.getItemCount() < 1)
                {
                    Item insertedDocument = InsertDocumentUsingSQL(sourceInnovator, destinationInnovator, document_item);
                }

                /** If Document is already existed in Destination **/

                /** (1) Same ID  - DO NOTHING **/
                /** (2) Different ID - UPDATE AND INSERT  **/
                if (documentItemDestination.getItemCount() > 0)
                {

                    string china_id = documentItemDestination.getItemByIndex(0).getID();

                    if (d_id != china_id)
                    {

                        StringBuilder sb_document_update = new StringBuilder("");
                        sb_document_update.Append(" UPDATE innovator.Document");
                        sb_document_update.Append(" SET [ARAS:UNIQUENESS_HELPER] = '" + china_id + "'" + " ,is_current = 0");
                        sb_document_update.Append(" WHERE ID = '" + china_id + "'");
                        string sql_sb_document_update = sb_document_update.ToString();
                        Item document_update_result = destinationInnovator.applySQL(sql_sb_document_update);
                        if (document_update_result.isError())
                        {
                            throw (new Exception("Error: AddDocument function for " + docItemNumber + " Error Detail:\n" + document_update_result.getErrorDetail()));
                        }



                        /********************* INSERT document ********************/
                        InsertDocumentUsingSQL(sourceInnovator, destinationInnovator, document_item);


                        /** update places where used with old_Id to be latest **/
                        StringBuilder sb_partDocument = new StringBuilder("");
                        sb_partDocument.Append("	SELECT * ");
                        sb_partDocument.Append("	FROM innovator.[part_document]");
                        sb_partDocument.Append(" WHERE related_id = '" + china_id + "'");
                        string sql_sb_partDocument = sb_partDocument.ToString();
                        Item partDocument = destinationInnovator.applySQL(sql_sb_partDocument);


                        var partDocument_count = partDocument.getItemCount();
                        for (int j = 0; j < partDocument_count; j++)
                        {
                            var partDocument_Item = partDocument.getItemByIndex(j);
                            partDocument_ID = partDocument_Item.getProperty("id");

                            /********************* UPDATE PART_DOCUMENT ********************/


                            StringBuilder sb_oldDocument_allupdate = new StringBuilder("");
                            sb_oldDocument_allupdate.Append(" UPDATE innovator.part_document");
                            sb_oldDocument_allupdate.Append(" SET related_id = '" + d_id + "'");
                            sb_oldDocument_allupdate.Append(" WHERE id = '" + partDocument_ID + "'");
                            string sql_sb_oldDocument_allupdate = sb_oldDocument_allupdate.ToString();
                            Item oldDocument_allupdate_result = destinationInnovator.applySQL(sql_sb_oldDocument_allupdate);


                        }
                        //Item deleteChianDocumentfile = destinationInnovator.applySQL("Delete FROM innovator.[document_file]" + " WHERE source_id='" + _id_ + "'" );
                        //Item deleteChinaDocument =  destinationInnovator.applySQL("Delete FROM innovator.[document]" + " WHERE item_number='" + docItemNumber + "'" + " AND is_current=0 ");
                    }
                }

                Item _documentItem = sourceInnovator.newItem("Document", "get");
                _documentItem.setAttribute("select", "id");
                _documentItem.setID(d_id);

                Item documentFileItem = sourceInnovator.newItem("Document File", "get");
                documentFileItem.setAttribute("select", "related_id(id,filename)");
                _documentItem.addRelationship(documentFileItem);

                Item resultDocuments = _documentItem.apply();

                if (resultDocuments.isError())
                {
                    return sourceInnovator.newError("Item not found: " + resultDocuments.getErrorDetail());
                }

                Item df_Items = resultDocuments.getRelationships();
                int df_count = df_Items.getItemCount();
                for (int j = 0; j < df_count; ++j)
                {

                    Item df_item = df_Items.getItemByIndex(j);
                    Item related_fileItem = df_item.getRelatedItem();

                    string fileID = related_fileItem.getID();
                    string file_Name = related_fileItem.getProperty("filename", "");

                    Item _getFiles = destinationInnovator.applySQL("select * From innovator.[FILE]  where id ='" + fileID + "'");
                    Item getFiles = sourceInnovator.applySQL("select * From innovator.[FILE]  where id ='" + fileID + "'");
                    string fileName = "";
                    Item addFile = destinationInnovator.newItem();
                    if (getFiles.getItemCount() > 0)
                    {

                        fileName = getFiles.getProperty("filename");
                    }

                    string filePath = FileDownloadAndUpload(sourceInnovator, fileID);

                    if (!string.IsNullOrEmpty(filePath))
                    {
                        if (_getFiles.getItemCount() < 1)
                        {
                            addFile = destinationInnovator.newItem("File", "add");

                            addFile.setID(fileID);
                            addFile.setProperty("filename", fileName);
                            addFile.attachPhysicalFile(filePath);
                            addFile = addFile.apply();
                            if (addFile.isError())
                            {
                                return sourceInnovator.newError("Error at attach physical file into China.");
                            }
                        }
                    }

                    string itemID = df_item.getProperty("id", "");

                    StringBuilder sb_DFile = new StringBuilder("");

                    sb_DFile.Append("	SELECT * ");
                    sb_DFile.Append("	FROM innovator.[document_file]");
                    sb_DFile.Append(" WHERE id = '" + itemID + "'" + " AND is_current=1");
                    string sql_sb_DFile = sb_DFile.ToString();
                    Item DFile = destinationInnovator.applySQL(sql_sb_DFile);
                    if (DFile.getItemCount() < 1)
                    {
                        InsertItemUsingSQL(sourceInnovator, destinationInnovator, df_item);
                    }
                }
            }
            return ChinaDocument;
        }
        public static Item AddVendor(Innovator sourceInnovator, Innovator destinationInnovator, Item sourcePart)
        {
            //  Innovator destinationInnovator = GetDestinationInnovator(sourceInnovator);

            string vendor_id = "";
            int V_Itemcount = 0;

            Item ChinaVendor = destinationInnovator.newItem();
            string SGParent_partID = sourcePart.getItemByIndex(0).getID();
            string partID = sourcePart.getID();

            // Item vendorItem = sourceInnovator.applySQL("select * From k_vendor where id in (select related_id From part_vendor where source_id = '" + partID + "')");
            StringBuilder sb_vendorItem = new StringBuilder("");
            sb_vendorItem.Append("	SELECT * ");
            sb_vendorItem.Append("	FROM innovator.k_vendor");
            sb_vendorItem.Append(" WHERE ID IN (");
            sb_vendorItem.Append("	SELECT related_id ");
            sb_vendorItem.Append("	  FROM innovator.part_vendor");
            sb_vendorItem.Append("		WHERE source_id = '" + partID + "')");
            string sql_sb_vendorItem = sb_vendorItem.ToString();
            Item vendorItem = sourceInnovator.applySQL(sql_sb_vendorItem);

            V_Itemcount = vendorItem.getItemCount();
            for (int c = 0; c < V_Itemcount; c++)
            {
                Item vendor_item = vendorItem.getItemByIndex(c);
                string VendorName = vendor_item.getProperty("k_name");
                string VendorID = vendor_item.getID();
                Item VItem = sourceInnovator.getItemById("k_vendor", VendorID);
                //  Item documentItemChina = destinationInnovator.applySQL("select * From k_vendor where is_current = 1 and id = '" + VendorID + "'");

                StringBuilder sb_same_id_item = new StringBuilder("");

                sb_same_id_item.Append(" SELECT * ");
                sb_same_id_item.Append(" FROM innovator.k_vendor");
                sb_same_id_item.Append(" WHERE ID = '" + VendorID + "'");
                string sql_sb_same_id_item = sb_same_id_item.ToString();
                Item v_item = destinationInnovator.applySQL(sql_sb_same_id_item);

                if (v_item.getItemCount() < 1)
                {
                    InsertVendorUsingSQL(sourceInnovator, destinationInnovator, VItem);
                }
                else
                {
                    // UpdateItemUsingSQL(sourceInnovator, destinationInnovator, vendor_item);
                    List<Item> propertyList = GetPropertyItems(sourceInnovator, "k_vendor");
                    Item S_vendorItem_added = sourceInnovator.applySQL("select * From k_vendor where is_current = 1 and id = '" + VendorID + "'");
                    Item vendorItem_added = destinationInnovator.applySQL("select * From k_vendor where is_current = 1 and id = '" + VendorID + "'");
                    if (vendorItem_added.getItemCount() > 0)
                    {
                        vendor_id = vendorItem_added.getProperty("id");
                    }
                    StringBuilder sb_update = new StringBuilder("");
                    sb_update.Append("UPDATE innovator.[k_vendor] SET ");

                    for (int i = 0; i < propertyList.Count; i++)
                    {
                        Item propertyItem = propertyList[i];
                        string dataType = propertyItem.getProperty("data_type", "");
                        string columnName = propertyItem.getProperty("keyed_name");
                        string columnValue = S_vendorItem_added.getProperty(columnName, "").Replace("'", "''");
                        if (columnValue != "")
                        {

                            if (i == propertyList.Count - 1)
                            {
                                sb_update.Append("[" + columnName + "] = '" + columnValue + "'");
                            }
                            else
                            {
                                sb_update.Append("[" + columnName + "] = '" + columnValue + "',");
                                // sb_update = sb_update.Substring(0, sb_update.Length - 1);

                            }
                        }
                    }
                    TrimEnd(sb_update, ',');
                    sb_update.Append(" WHERE ID = '" + vendor_id + "'");
                    string sql_update = sb_update.ToString();
                    Item updateItem = destinationInnovator.applySQL(sql_update);
                    if (updateItem.isError())
                    {
                        return destinationInnovator.newError("error: " + updateItem.getErrorDetail());
                    }
                }


            }
            return ChinaVendor;

        }
        static void TrimEnd(StringBuilder sb_update, char letter)
        {

            if (sb_update[sb_update.Length - 1] == letter)
            {
                sb_update.Length -= 1;
            }
        }

        public static Item InsertDocumentUsingSQL(Innovator sourceInnovator, Innovator destinationInnovator, Item item)
        {
            string query1 = "INSERT INTO Document(";
            string query2 = "VALUES(";
            string itemID = item.getProperty("id", "");
            string itemKeyedName = item.getProperty("keyed_name", "");

            List<Item> propertyList = GetPropertyItems(sourceInnovator, "document");

            Item chinaItem = destinationInnovator.newItem("Document", "add");
            for (int i = 0; i < propertyList.Count; i++)
            {

                Item propertyItem = propertyList[i];
                string dataType = propertyItem.getProperty("data_type", "");
                string columnName = propertyItem.getProperty("keyed_name");

                if (i != propertyList.Count - 1 && item.getProperty(columnName) != "" && item.getProperty(columnName) != null)
                {
                    query1 += columnName + ",";
                }
                else if (i == propertyList.Count - 1 && item.getProperty(columnName) != "" && item.getProperty(columnName) != null)
                {
                    query1 += columnName + ")";
                }
                else if (i == propertyList.Count - 1 && (item.getProperty(columnName) == "" || item.getProperty(columnName) == null))
                {
                    query1 = query1.Substring(0, query1.Length - 1);
                    query1 += ")";
                }
            }
            for (int i = 0; i < propertyList.Count; i++)
            {
                Item propertyItem = propertyList[i];
                string dataType = propertyItem.getProperty("data_type", "");
                string columnName = propertyItem.getProperty("keyed_name");

                if (i != propertyList.Count - 1 && item.getProperty(columnName) != "" && item.getProperty(columnName) != null)
                {
                    query2 += "'" + item.getProperty(columnName).Replace("'", "''") + "',";
                }

                else if (i == propertyList.Count - 1 && item.getProperty(columnName) != "" && item.getProperty(columnName) != null)
                {
                    query2 += "'" + item.getProperty(columnName).Replace("'", "''") + "')";
                }

                else if (i == propertyList.Count - 1 && (item.getProperty(columnName) == "" || item.getProperty(columnName) == null))
                {
                    query2 = query2.Substring(0, query2.Length - 1);
                    query2 += ")";
                }
            }
            query1 += " " + query2;
            chinaItem = destinationInnovator.applySQL(query1);
            if (chinaItem.isError())
            {
                var mess = chinaItem.getErrorDetail();
                throw (new Exception("Error: InsertDocumentUsingSQL function for " + itemKeyedName + " Error Detail:\n" + chinaItem.getErrorDetail()));

            }
            var chinaItem_count = chinaItem.getItemCount();

            return chinaItem;
        }
        public static Item InsertVendorUsingSQL(Innovator sourceInnovator, Innovator destinationInnovator, Item item)
        {
            string query1 = "INSERT INTO k_vendor(";
            string query2 = "VALUES(";
            string itemID = item.getProperty("id", "");
            string itemVendorName = item.getProperty("k_name", "");
            string itemTypeName = item.getType();

            List<Item> propertyList = GetPropertyItems(sourceInnovator, "k_vendor");
            Item chinaItem = destinationInnovator.newItem("k_vendor", "add");
            for (int i = 0; i < propertyList.Count; i++)
            {
                Item propertyItem = propertyList[i];
                string dataType = propertyItem.getProperty("data_type", "");
                string columnName = propertyItem.getProperty("keyed_name");

                if (i != propertyList.Count - 1 && item.getProperty(columnName) != "" && item.getProperty(columnName) != null)
                {
                    query1 += columnName + ",";
                }
                else if (i == propertyList.Count - 1 && item.getProperty(columnName) != "" && item.getProperty(columnName) != null)
                {
                    query1 += columnName + ")";
                }
                else if (i == propertyList.Count - 1 && (item.getProperty(columnName) == "" || item.getProperty(columnName) == null))
                {
                    query1 = query1.Substring(0, query1.Length - 1);
                    query1 += ")";
                }
            }
            for (int i = 0; i < propertyList.Count; i++)
            {
                Item propertyItem = propertyList[i];
                string dataType = propertyItem.getProperty("data_type", "");
                string columnName = propertyItem.getProperty("keyed_name");

                if (i != propertyList.Count - 1 && item.getProperty(columnName) != "" && item.getProperty(columnName) != null)
                {
                    query2 += "'" + item.getProperty(columnName).Replace("'", "''") + "',";
                }

                else if (i == propertyList.Count - 1 && item.getProperty(columnName) != "" && item.getProperty(columnName) != null)
                {
                    query2 += "'" + item.getProperty(columnName).Replace("'", "''") + "')";
                }

                else if (i == propertyList.Count - 1 && (item.getProperty(columnName) == "" || item.getProperty(columnName) == null))
                {
                    query2 = query2.Substring(0, query2.Length - 1);
                    query2 += ")";
                }
            }
            query1 += " " + query2;
            chinaItem = destinationInnovator.applySQL(query1);
            if (chinaItem.isError())
            {
                var mess = chinaItem.getErrorDetail();
            }
            var chinaItem_count = chinaItem.getItemCount();

            return chinaItem;
        }

        public static Item InsertPartUsingSQL(Innovator sourceInnovator, Innovator destinationInnovator, Item item)
        {
            string query1 = "INSERT INTO Part(";
            string query2 = "VALUES(";
            string itemID = item.getProperty("id", "");
            string itemKeyedName = item.getProperty("item_number", "");

            List<Item> propertyList = GetPropertyItems(sourceInnovator, "part");

            Item chinaItem = destinationInnovator.newItem("Part", "add");
            for (int i = 0; i < propertyList.Count; i++)
            {
                Item propertyItem = propertyList[i];
                string dataType = propertyItem.getProperty("data_type", "");
                string columnName = propertyItem.getProperty("keyed_name");

                if (i != propertyList.Count - 1 && item.getProperty(columnName) != "" && item.getProperty(columnName) != null)
                {
                    query1 += columnName + ",";
                }
                else if (i == propertyList.Count - 1 && item.getProperty(columnName) != "" && item.getProperty(columnName) != null)
                {
                    query1 += columnName + ")";
                }

                else if (i == propertyList.Count - 1 && (item.getProperty(columnName) == "" || item.getProperty(columnName) == null))
                {
                    query1 = query1.Substring(0, query1.Length - 1);
                    query1 += ")";
                }
            }
            for (int i = 0; i < propertyList.Count; i++)
            {
                Item propertyItem = propertyList[i];
                string dataType = propertyItem.getProperty("data_type", "");
                string columnName = propertyItem.getProperty("keyed_name");
                if (dataType == "string")
                {
                    if (i != propertyList.Count - 1 && item.getProperty(columnName) != "" && item.getProperty(columnName) != null)
                    {
                        query2 += "N'" + item.getProperty(columnName).Replace("'", "''") + "',";
                    }

                    else if (i == propertyList.Count - 1 && item.getProperty(columnName) != "" && item.getProperty(columnName) != null)
                    {
                        query2 += "N'" + item.getProperty(columnName).Replace("'", "''") + "')";
                    }

                    else if (i == propertyList.Count - 1 && (item.getProperty(columnName) == "" || item.getProperty(columnName) == null))
                    {
                        query2 = query2.Substring(0, query2.Length - 1);
                        query2 += ")";
                    }
                }
                else
                {
                    if (i != propertyList.Count - 1 && item.getProperty(columnName) != "" && item.getProperty(columnName) != null)
                    {
                        query2 += "'" + item.getProperty(columnName).Replace("'", "''") + "',";
                    }

                    else if (i == propertyList.Count - 1 && item.getProperty(columnName) != "" && item.getProperty(columnName) != null)
                    {
                        query2 += "'" + item.getProperty(columnName).Replace("'", "''") + "')";
                    }

                    else if (i == propertyList.Count - 1 && (item.getProperty(columnName) == "" || item.getProperty(columnName) == null))
                    {
                        query2 = query2.Substring(0, query2.Length - 1);
                        query2 += ")";
                    }
                }
            }
            query1 += " " + query2;
            chinaItem = destinationInnovator.applySQL(query1);
            if (chinaItem.isError())
            {
                var mess = chinaItem.getErrorDetail();
                throw (new Exception("Error: InsertPartUsingSQL function for " + itemKeyedName + " Error Detail:\n" + chinaItem.getErrorDetail()));
            }

            var chinaItem_count = chinaItem.getItemCount();

            return chinaItem;
        }

        public static List<Item> GetPropertyItems(Innovator SourceInnovator, string itemTypeName)
        {
            List<Item> propertyList = new List<Item>();
            //if (itemTypeName == "Part_BOM") itemTypeName = "Part BOM";

            if (itemTypeName == "Part_BOM")
            {
                itemTypeName = "Part BOM";
            }
            else if (itemTypeName == "Part_Document")
            {
                itemTypeName = "Part Document";
            }
            else if (itemTypeName == "Document_File")
            {
                itemTypeName = "Document File";
            }
            else if (itemTypeName == "Part_Vendor")
            {
                itemTypeName = "Part Vendor";
            }
            Item itemType = SourceInnovator.newItem("ItemType", "get");
            itemType.setAttribute("select", "name");
            itemType.setProperty("name", itemTypeName);
            Item property = SourceInnovator.newItem("Property", "get");
            // property.setAttribute("select", "keyed_name");
            itemType.addRelationship(property);
            Item resultItemType = itemType.apply();

            if (resultItemType.isError())
            {
                throw (new Exception("Error at Getting ItemType " + itemTypeName));
            }

            Item resultProperties = resultItemType.getRelationships();
            int propertiesCount = resultProperties.getItemCount();
            for (int i = 0; i < propertiesCount; i++)
            {
                Item propertyItem = resultProperties.getItemByIndex(i);
                propertyList.Add(propertyItem);
            }
            return propertyList;
        }

        public static Item UpdateItemUsingSQL(Innovator SourceInnovator, Innovator DestinationInnovator, Item sourceItem)
        {
            Item returnItem = SourceInnovator.newItem();
            string itemID = sourceItem.getID();
            string itemKeyedName = sourceItem.getProperty("keyed_name", "");
            string itemTypeName = sourceItem.getType();
            const string part_Doc = "Part Document";

            List<Item> propertyList = GetPropertyItems(SourceInnovator, itemTypeName);

            Item chinaItem = DestinationInnovator.newItem(itemTypeName, "get");
            if (itemTypeName != "Part BOM" && itemTypeName != part_Doc && itemTypeName != "Part Vendor")
            {
                StringBuilder sb_get_config_id = new StringBuilder("");
                sb_get_config_id.Append(" select * ");
                sb_get_config_id.Append(" from " + itemTypeName);
                sb_get_config_id.Append(" where id = '" + itemID + "'");
                string sql_get_config_id = sb_get_config_id.ToString();
                Item get_config_id = SourceInnovator.applySQL(sql_get_config_id);
                string config_id = get_config_id.getProperty("config_id", "");
                chinaItem.setProperty("config_id", config_id);
            }
            if (itemTypeName == "Part BOM" || itemTypeName == "Part Document" || itemTypeName == "Part Vendor")
            {
                chinaItem.setProperty("keyed_name", itemKeyedName);
            }
            chinaItem.setProperty("is_current", "1");
            chinaItem = chinaItem.apply();
            if (!chinaItem.isError() && chinaItem.getItemCount() > 0)
            {
                string chinaID = chinaItem.getID();

                StringBuilder sb_select = new StringBuilder("");
                sb_select.Append("SELECT ");
                for (int i = 0; i < propertyList.Count; i++)
                {
                    Item propertyItem = propertyList[i];
                    string dataType = propertyItem.getProperty("data_type", "");
                    string columnName = propertyItem.getProperty("keyed_name", "");
                    switch (dataType)
                    {
                        case "date":
                            {
                                columnName = "CAST(" + columnName + " AS DATETIME2) AS " + columnName;
                            }
                            break;

                        default:
                            break;
                    }
                    if (i == propertyList.Count - 1)
                    {
                        sb_select.Append(columnName);
                    }
                    else
                    {
                        sb_select.Append(columnName + ",");
                    }
                }

                sb_select.Append(" FROM [innovator].[" + itemTypeName + "]");
                sb_select.Append("WHERE ID = '" + itemID + "'");
                string sql_select = sb_select.ToString();
                Item sqlItem = SourceInnovator.applySQL(sql_select);
                if (sqlItem.isError())
                {
                    throw new Exception("Error : SQL_SELECT");
                }
                // Update [ARAS_UNIQUENESS_HELPER]
                // TODO: Check target Item Is_Versionable

                Item itemType = SourceInnovator.newItem("ItemType", "get");
                itemType.setAttribute("select", "is_versionable");
                itemType.setProperty("keyed_name", itemTypeName);
                itemType = itemType.apply();
                if (!itemType.isError() && itemType.getItemCount() > 0)
                {
                    string is_versionable = itemType.getProperty("is_versionable", "");
                    if (is_versionable == "0")
                    {
                        StringBuilder sb_update = new StringBuilder("");
                        sb_update.Append("UPDATE [innovator].[" + itemTypeName + "] SET ");

                        //loop for column names
                        for (int i = 0; i < propertyList.Count; i++)
                        {
                            Item propertyItem = propertyList[i];
                            string dataType = propertyItem.getProperty("data_type", "");
                            string columnName = propertyItem.getProperty("keyed_name");
                            string columnValue = sqlItem.getProperty(columnName, "").Replace("'", "''");
                            if (columnValue != "")
                            {
                                switch (dataType)
                                {
                                    case "date":
                                    case "string":
                                    case "item":
                                    case "list":
                                    case "filter list":
                                    case "mv_list":
                                    case "text":
                                    case "formatted text":
                                    case "color":
                                    case "color list":
                                    case "image":
                                    case "md5":
                                    case "federated":
                                    case "ml_string":
                                    case "sequence":
                                        {
                                            columnValue = "'" + columnValue + "'";
                                        }
                                        break;

                                    case "decimal":
                                    case "float":
                                    case "integer":
                                        break;

                                    default:
                                        break;
                                }
                                if (i == propertyList.Count - 1)
                                {
                                    sb_update.Append("[" + columnName + "] = " + columnValue);
                                }
                                else
                                {
                                    sb_update.Append("[" + columnName + "] = " + columnValue + ",");
                                }
                            }
                        }

                        sb_update.Append(" WHERE ID = '" + chinaID + "'");
                        string sql_update = sb_update.ToString();
                        Item updateItem = DestinationInnovator.applySQL(sql_update);
                        if (updateItem.isError())
                        {
                            throw new Exception("Error : UPDATE : " + itemKeyedName);
                        }
                    }

                    if (is_versionable == "1")
                    {
                        // UPDATE : [ARAS_UNIQUENESS_HELPER]
                        StringBuilder sb_uniqueness = new StringBuilder("");
                        sb_uniqueness.Append("UPDATE [innovator].[" + itemTypeName + "] SET ");
                        sb_uniqueness.Append("[ARAS:UNIQUENESS_HELPER] = '" + chinaID + "'");
                        sb_uniqueness.Append(" ,is_current = 0");
                        sb_uniqueness.Append(" WHERE ID = '" + chinaID + "'");
                        string sql_uniqueness = sb_uniqueness.ToString();
                        Item uniquenessResult = DestinationInnovator.applySQL(sql_uniqueness);
                        if (uniquenessResult.isError())
                        {
                            throw new Exception("Error : UPDATE [ARAS:UNIQUENESS_HELPER]");
                        }

                        // ADD NEW RECORD
                        InsertItemUsingSQL(SourceInnovator, DestinationInnovator, sourceItem);

                        /*** update part is used in destination (part BOM)  ***/

                        StringBuilder sb_bom_update = new StringBuilder("");
                        sb_bom_update.Append(" UPDATE innovator.PART_BOM");
                        sb_bom_update.Append(" SET related_id = '" + itemID + "'");
                        sb_bom_update.Append(" WHERE ID IN (");
                        sb_bom_update.Append("	SELECT ID");
                        sb_bom_update.Append("	FROM innovator.PART_BOM");
                        sb_bom_update.Append("	WHERE related_id IN (");
                        sb_bom_update.Append("		SELECT ID");
                        sb_bom_update.Append("		FROM innovator.PART");
                        sb_bom_update.Append("		WHERE KEYED_NAME = '" + itemKeyedName + "'))");
                        string sql_bom_update = sb_bom_update.ToString();
                        Item bom_update_result = DestinationInnovator.applySQL(sql_bom_update);
                        if (bom_update_result.isError())
                        {
                            throw new Exception("Error : BOM UPDATE");
                        }
                    }
                }
            }
            return returnItem;
        }
    }
}